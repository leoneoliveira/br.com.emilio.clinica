<?php include 'head.php'; ?>
<?php include 'conexao/config.php' ?>
<body class="cl-default fixed">

    <!-- inicio:navbar top -->
    <?php include 'head_menu_top.php'; ?>
    <!-- Fin:navbar top -->    

    <!-- inicio: Toda Lateral do menu -->
    <?php include 'head_menu_left.php'; ?>
    <!-- fim: Toda Lateral do menu -->

    <!--lib. http://plentz.github.io/jquery-maskmoney/ -->
    <script src="js/jquery.maskMoney.min.js"></script>

    <script src="js/script-cadastrar-solicitacao.js"></script>

    <style>
    .form-group{
        padding-left:10px;
        padding-right: 10px;
    }
    </style>

    <?php 

    $query = "SELECT * FROM exame WHERE status = 'A'";
    $result = mysqli_query($conn, $query);
    $total_num_rows = mysqli_num_rows($result);

    $queryMed = "SELECT * FROM medico order by nome ";
    $resultMed = mysqli_query($conn, $queryMed);

    ?>    

    <aside class="right-side">
        <section class="content">
            <h1>
                Solicitações de Exames
            </h1>
            <!-- start:breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-plus-square"></i> Solicitação</a></li>
                <li class="active">Nova Solicitações de Exames</li>
            </ol>
            <!-- end:breadcrumb -->

            <!-- start:content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box blank-page">

                    <div class="alert alert-warning" role="alert"><b>ATENÇÃO!</b> o agendamento sera confirmado via telefone.</div>

                        <form class="form-horizontal tasi-form" id="form_solic" method="post"
                        accept-charset="UTF-8" enctype="application/x-www-form-urlencoded"
                        autocomplete="off" >

                        <input type="hidden" name="controle" id="controle" value="N">
                        <fieldset>
                            <!-- Form Name -->
                            <legend>Exame</legend>

                            <div class="row">
                                <div class="col-md-10">

                                  <section class="panel">

                                    <div class="panel-body">

                                            <div class="row">
                                              <div class="col-md-6">
                                            <!--     <div class="form-group">
                                                    <label for="valor">Medico Solicitante</label>
                                                    <input type="text" class="form-control" name="nome_medico" placeholder="Digite o nome do medico solicitante" required >
                                                </div> -->

                                                <div class="form-group">
                                                      <label for="estado">Médico Solicitante</label>
                                                      <select class="form-control" name="nome_medico"  required>
                                                        <option value="">Selecione o médico</option>  
                                                        <?php while($row = mysqli_fetch_array($resultMed)){ ?>                                                      
                                                        <option value="<?php echo utf8_encode($row['cd_medico']); ?>">
                                                            <?php echo utf8_encode($row['nome']); ?>
                                                        </option>
                                                        <?php } ?>
                                                  </select>
                                              </div>

                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                <label for="valor">Data e Hora</label>
                                                <input type="text" id="dataInicial" name="data_hora" class="form-control" placeholder="Digite a data e hora para realização do exame" required>
                                            </div>  
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="valor">Paciente</label>
                                        <input type="text" name="nome_paciente" class="form-control" placeholder="Digite o nome do paciente" required>
                                    </div>




                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="valor">CPF</label>
                                            <input type="text" name="cpf_paciente" class="form-control cpf" placeholder="Digite o CPF do paciente" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6"> 
                                    </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="valor">Telefone</label>
                                        <input type="text" name="telefone" class="form-control telefone" placeholder="Digite o telefone do paciente" required>
                                    </div>
                                </div>
                                <div class="col-md-6"> 
                                    <div class="form-group">
                                        <label for="valor">Celular</label>
                                        <input type="text" name="celular" class="form-control celular" placeholder="Digite o celular do paciente" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6"> 
                                    <div class="form-group">
                                        <label for="valor">Exames</label>
                                        <div class="input-group">  
                                          <select class="form-control m-bot15" id="exame_select" >
                                            <option value="">Selecione o exame</option>
                                            <?php while($row = mysqli_fetch_array($result)){ ?>
                                            <option value="<?php echo utf8_encode($row['cd_exame']); ?>">
                                                <?php echo utf8_encode($row['nome']); ?>
                                            </option>
                                            <?php } ?>
                                            
                                        </select>
                                        <div class="input-group-btn">
                                            <button class="btn btn-success btn_add_exame">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            <input type="hidden" id="exames" name="exames">

                            </div>
                            <div class="col-md-6"> 
                                <div class="form-group">
                                    <label for="valor">Lista de exames</label>
                                    <!-- Aqui vai ficar a lista de exames  -->
                                    <div class="box" style="padding: 0px;">
                                        <ul class="list-group list-group-item-square lista-exames">
                                            
                                        </ul>
                                </div>                                    

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="descricao">Observação</label>
                        <textarea class="form-control" id="descricao" name="descricao" maxlength="500" required  rows="5"></textarea>
                    </div>


                    <div align="right">
                        <button class="btn btn-danger btn_reset">Cancelar</button>
                        <button type="submit" class="btn btn-success">Salvar</button>

                    </div>


              

            </div>
        </section>


    </div>
</div>

</fieldset>
</form>





</div>
</div>
</div>
<!-- end:content -->

</section>
</aside>
<!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->


<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h4>
            </div>
            <div class="modal-body" align="center">
                <h1 id="msg_modal" style="color: #2ECC71;">   </h1>
            </div>
            <div class="modal-footer"  align="right">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>
<!-- end modal  -->



</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Oct 2015 00:45:02 GMT -->
</html>