  
<?php 
include 'conexao/config.php';
$var = $_SESSION['usr_nome'];
$nome = explode(" ", $var);

$cd_perfil = $_SESSION['usr_perfil'];
$cd_user = $_SESSION['usr_cd_user'];


$query_user = "SELECT * FROM user WHERE cd_user = '$cd_user' ";
$result_menu = mysqli_query($conn, $query_user);
$row_use_menu = mysqli_fetch_array($result_menu);
?>

    <header class="header">
        <a href="#" class="logo">
          Clinica
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="navbar-btn sidebar-toggle" data-target="#atas" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="collapse navbar-collapse" id="atas">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php  
                                $img_perfil = 'uploads_foto_perfil/'.utf8_encode($row_use_menu['img_perfil']);
                                if (file_exists($img_perfil)) {
                            ?>  
                                  <img src="uploads_foto_perfil/<?php echo $row_use_menu['img_perfil'] ?>" class="img-circle img-responsive img-user">
                                
                            <?php    
                                } else {
                                    echo '<img src="img/img_sem.jpg"  class="img-circle img-responsive img-user" >';
                                }
                            ?>                                 
                            <strong>@<?php echo ucfirst($nome[0]); ?></strong>
                            </a>
                            <ul class="dropdown-menu dropdown-login">
                                <li>
                                    <div class="navbar-login">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <p class="text-center">
                                                <?php  
                                                    $img_perfil = 'uploads_foto_perfil/'.utf8_encode($row_use_menu['img_perfil']);
                                                    if (file_exists($img_perfil)) {
                                                ?>  
                                                      <img src="uploads_foto_perfil/<?php echo $row_use_menu['img_perfil'] ?>" style="width:50px;height:50px;max-width: none;" class="img-responsive img-circle">
                                                    
                                                <?php    
                                                    } else {
                                                        echo '<img src="img/img_sem.jpg" style="width:50px;height:50px;max-width: none;" class="img-responsive img-circle">';
                                                    }
                                                ?> 
                                                </p>
                                            </div>
                                            <!-- Perfil de Administrativo -->
                                             
                                            <div class="col-lg-9">
                                                <p class="text-left"><strong><?php echo ucfirst($nome[0]); ?></strong></p>
                                                <p class="text-left small"><?php  echo $row_use_menu['email']; ?></p>
                                                <?php if ($cd_perfil == 1 || $cd_perfil == 2): ?>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <small><a href="alterar_usuario.php?cd_user=<?php echo $row_use_menu['cd_user']?>">Editar perfil</a></small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        
                                                    </div>
                                                </div>
                                                <?php endif ?>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="navbar-login navbar-login-session">
                                        <a href="destroy.php" class="btn btn-default btn-square btn-block">Sair</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>