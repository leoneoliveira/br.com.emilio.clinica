<?php include 'verifica.php'; ?>	
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<title>Clinica </title>
	<!-- start:bootstrap v3.2.0 -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<!-- start:font awesome v4.1.0 -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<!-- start:bootstrap reset -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap-reset.css">
	<!-- start:style arjuna -->
	<link rel="stylesheet" type="text/css" href="css/arjuna.css">
	<!-- Calendario para solicitaçao de exame -->
	<link  rel="stylesheet" href="css/bootstrap-datetimepicker.css">


			<script src="js/moment-with-locales.js" ></script>
			<script type="text/javascript">
					moment.locale('pt-br');
			</script>

      <!-- start:javascript for all pages -->
      <!-- start:jquery -->
      <script src="js/jquery-1.11.1.min.js"></script>
      <!-- start:bootstrap -->
      <script src="js/bootstrap.min.js"></script>
      <!-- start:arjuna.js -->
      <script src="js/arjuna.js"></script>

      <!-- Calendario para solicitação de exame -->
      <script src="plugins/calendario/moment.js" type="text/javascript" charset="utf-8"></script>
      <script src="plugins/calendario/bootstrap-datetimepicker.min.js" type="text/javascript" charset="utf-8"></script>
      <script src="plugins/calendario/traducao.js" type="text/javascript" charset="utf-8"></script>

      <!-- end:javascript for all pages-->
			<script src="js/jquery.validate.min.js"></script>
			<script type="text/javascript">
					//configura para o jquery validate para o bootstrap
					$.validator.setDefaults({
							highlight: function(element) {
									$(element).closest('.form-group').addClass('has-error');
							},
							unhighlight: function(element) {
									$(element).closest('.form-group').removeClass('has-error');
							},
							errorElement: 'span',
							errorClass: 'help-block',
							errorPlacement: function(error, element) {
									if(element.parent('.input-group').length) {
											error.insertAfter(element.parent());
									} else {
											error.insertAfter(element);
									}
							}
						});
			</script>

			<script src="js/jquery.mask.min.js"></script>

	

    </head>
