<?php include 'head.php'; ?>
<body class="cl-default fixed">

    <!-- inicio:navbar top -->
    <?php include 'head_menu_top.php'; ?>
    <!-- Fin:navbar top -->

    <!-- inicio: Toda Lateral do menu -->
    <?php include 'head_menu_left.php'; ?>
    <!-- fim: Toda Lateral do menu -->

    <script src="js/script-cadastro-medico.js"></script> 

    <style>
    .form-group{
        padding-left: 10px;
        padding-right: 10px;
    }
    </style>

    <aside class="right-side">
        <section class="content">
            <h1>
                Cadastro Médico
            </h1>
            <!-- start:breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-stethoscope"></i> Médico</a></li>
                <li class="active">Novo médico</li>
            </ol>
            <!-- end:breadcrumb -->

            <!-- start:content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box blank-page">


                        <form class="form-horizontal tasi-form" id="form_medico"
                          method="post"  accept-charset="UTF-8"
                          enctype="application/x-www-form-urlencoded"  autocomplete="off" >
                          <input type="hidden" name="controle" value="N">
                            <fieldset>
                                <!-- Form Name -->
                                <legend>Dados do perfil</legend>

                                <div class="row">
                                    <div class="col-md-10">

                                      <section class="panel">

                                        <div class="panel-body">
                                            <form class="form-horizontal" role="form">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o nome do médico" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="especialidade">Especialidade Médica</label>
                                                    <input type="text" class="form-control" id="especialidade" name="especialidade" placeholder="Digite a especialidade médica" required>
                                                </div>

                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="crm">CRM</label>
                                                        <input type="text" class="form-control" id="crm" name="crm" placeholder=" Digite o CRM do médico " required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="cpf">CPF</label>
                                                        <input type="text" class="form-control cpf"  id="cpf" name="cpf"  maxlength="14" placeholder="Digite o CPF" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="telefone">Telefone</label>
                                                    <input type="text" class="form-control telefone" id="telefone" name="telefone" placeholder=" Digite o telefone" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="celular">Celular</label>
                                                    <input type="text" class="form-control celular" id="celular" name="celular" placeholder=" Digite o celular" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Digite o email." required>
                                        </div>

                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="cep" >CEP</label>
                                                <input type="text" class="form-control cep" id="cep" name="cep" placeholder="Digite o cep" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label for="estado">Estado</label>
                                                <select class="form-control m-bot15" id="estado" name="estado" required>
                                                  <option value="">Selecione o estado</option>
                                                  <option value="AC">Acre</option>
                                                  <option value="AL">Alagoas</option>
                                                  <option value="AP">Amapá</option>
                                                  <option value="AM">Amazonas</option>
                                                  <option value="BA">Bahia</option>
                                                  <option value="CE">Ceará</option>
                                                  <option value="DF">Distrito Federal</option>
                                                  <option value="ES">Espirito Santo</option>
                                                  <option value="GO">Goiás</option>
                                                  <option value="MA">Maranhão</option>
                                                  <option value="MS">Mato Grosso do Sul</option>
                                                  <option value="MT">Mato Grosso</option>
                                                  <option value="MG">Minas Gerais</option>
                                                  <option value="PA">Pará</option>
                                                  <option value="PB">Paraíba</option>
                                                  <option value="PR">Paraná</option>
                                                  <option value="PE">Pernambuco</option>
                                                  <option value="PI">Piauí</option>
                                                  <option value="RJ">Rio de Janeiro</option>
                                                  <option value="RN">Rio Grande do Norte</option>
                                                  <option value="RS">Rio Grande do Sul</option>
                                                  <option value="RO">Rondônia</option>
                                                  <option value="RR">Roraima</option>
                                                  <option value="SC">Santa Catarina</option>
                                                  <option value="SP">São Paulo</option>
                                                  <option value="SE">Sergipe</option>
                                                  <option value="TO">Tocantins</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="endereco">Endereço</label>
                                        <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Digite o endereço" required>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="bairro">Bairro</label>
                                            <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Digite o bairro" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="cidade">Cidade</label>
                                            <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Digite a cidade" required>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="banco">Banco</label>
                                    <input type="text" class="form-control" id="banco" name="banco" placeholder="Digite o nome do banco" required>
                                </div>

                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="agencia">Agência</label>
                                        <input type="text" class="form-control" id="agencia" name="agencia" placeholder="Digite o número da agência" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="conta">Conta</label>
                                        <input type="text" class="form-control" id="conta" name="conta" placeholder="Digite o número da conta" required>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div align="right">
                                <button class="btn btn-danger btn_reset">Cancelar</button>
                                <button type="submit" class="btn btn-success">Salvar</button>
                            </div>




                        </form>

                    </div>
                </section>


            </div>
        </div>

    </fieldset>
</form>



</div>
</div>
</div>
<!-- end:content -->

</section>
</aside>
<!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->



<!-- modal  -->

<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h4>
      </div>
      <div class="modal-body" align="center">
        <h1 id="msg_modal" style="color: #2ECC71;">   </h1>
      </div>
      <div class="modal-footer"  align="right">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>

      </div>
    </div>
  </div>
</div>

<!-- end modal  -->






</body>

</html>
