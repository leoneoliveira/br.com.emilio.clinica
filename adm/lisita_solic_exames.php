<?php include 'head.php'; ?>
<?php include 'conexao/config.php' ?>

<body class="cl-default fixed">

    <link href="plugins/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />

    <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="css/table-responsive.css">

    <style>

    .warning {
        background-color: #F9EBA2 !important;
    }

    tr.warning > td {
        background-color: #F9EBA2 !important;
    }

    .danger{
        background-color: #F69E9E !important;
    }

    tr.danger > td {
        background-color: #F69E9E !important;
    }

    </style>


    <!-- inicio:navbar top -->
    <?php include 'head_menu_top.php'; ?>
    <!-- Fin:navbar top -->
    
    <!-- inicio: Toda Lateral do menu -parametro($vMenu) -->
    <?php include 'head_menu_left.php'; ?>
    <!--  Toda Lateral do menu -->


    <script src="plugins/data-tables/dataModificado/jquery.dataTables.js"></script>
    <script src="plugins/data-tables/dataModificado/dataTables.bootstrap.js"></script>


    <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {

        $('.a_menu_solic_left').click();

        
        $('#lista_situ_solic').dataTable( {
            "pageLength": 10,
            "lengthChange": false,  
                        "order": [[ 3, "desc" ]], //ordena por coluna 
                        "language": {
                            "url": "plugins/data-tables/dataModificado/Portuguese-Brasil.json" //tradução para português
                        },
                        stateSave: true, //salvar pesquisa em tempo 
                        "searching": true //oculta ou mostra
                    }); 


    } );
    </script>


    <?php 

    $query = "SELECT * FROM solicitacao where user_cad = '$cd_user' order by cd_solicitacao desc ";
    $result = mysqli_query($conn, $query);
    $total_num_rows = mysqli_num_rows($result);
    function situacao($args){
        switch ($args) {
            case 'A':
            $situacao =  '<span class="label label-success">Confirmado</span>';
            break;
            case 'I':
            $situacao = '<span class="label label-warning">Cancelado</span>';
            break;
            case 'E':
            $situacao = '<span class="label label-danger">Excluído</span>';
            break;
            case 'AC':
            $situacao = '<span class="label label-info">Pendente</span>';
            break;
            default:
            $situacao =  "";
        }

        echo $situacao;
    }

    function linhaStatus ($args){
        switch ($args) {
            case 'A':
            $class =  '';
            break;
            case 'I':
            $class = ' class="warning" ';
            break;
            case 'E':
            $class = ' class="danger" ';
            break;
            default:
            $class =  '';
        }

        echo $class;
    }

    function nome_medico($conn, $cd_medico){
    $queryMed = "SELECT * FROM medico where cd_medico = '$cd_medico' ";
    $resultMed = mysqli_query($conn, $queryMed);
    $row = mysqli_fetch_array($resultMed);
    echo $row['nome'];
    }

    ?>

    <aside class="right-side">
        <section class="content">
            <h1>
                Situação de Solicitação de Exame         
            </h1>
            <!-- start:breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-medkit"></i> Solicitações</a></li>
                <li class="active">Situação</li>
            </ol>
            <!-- end:breadcrumb -->

            <!-- start:content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box blank-page">
                        <div class="row">
                            <div class="col-md-12">
                                <section id="unseen">
                                    <table  class="display table table-bordered table-striped table-condensed" id="lista_situ_solic">
                                        <thead>
                                            <tr>
                                                <th width="15%">Data - Hora</th>
                                                <th width="35%">Paciente</th>
                                                <th width="35%">Méd. Solicitante</th>
                                                <th width="35%">Méd. Executante</th>
                                                <th width="10%">Situação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while($row = mysqli_fetch_array($result)){ ?>
                                            <tr>
                                                <td><?php echo $row['dt_solicitacao']; ?></td>
                                                <td><?php echo $row['nm_paciente']; ?></td>
                                                <td><?php nome_medico($conn, $row['cd_medico_solicitante']); ?></td>
                                                <td><?php nome_medico($conn, $row['cd_medico_executante']); ?></td>
                                                <td align="center"><?php  situacao($row['status']); ?></td>
                                            </tr> 
                                            <?php } ?>                                     
                                        </tbody>
                                    </table>
                                </section>
                            </div>                            
                        </div>





                    </div>
                </div>
            </div>
            <!-- end:content -->

        </section>
    </aside>
    <!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->


</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Oct 2015 00:45:02 GMT -->
</html>