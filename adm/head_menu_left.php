<?php include 'verifica.php'; ?>  
<!-- inicio: Funçoes -->
<?php include 'includes/saudacao.php'; ?>
<!-- fim: Funçoes -->
<?php include 'conexao/config.php'; ?>


<?php 

$var = $_SESSION['usr_nome'];
$nome = explode(" ", $var);


$cd_perfil = $_SESSION['usr_perfil'];
$cd_user = $_SESSION['usr_cd_user'];


$query_user = "SELECT * FROM user WHERE cd_user = '$cd_user' ";
$result_menu = mysqli_query($conn, $query_user);
$row_use_menu = mysqli_fetch_array($result_menu);
?>
<!-- start:wrapper body -->
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- start:left sidebar -->


    <aside class="left-side sidebar-offcanvas">
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">

                <!-- incio: foto + boas vindas -->
                <div class="tab-content">
                    <div class="tab-pane active" id="user">
                        <div class="image">

                        <?php  
                            $img_perfil = 'uploads_foto_perfil/'.utf8_encode($row_use_menu['img_perfil']);
                            if (file_exists($img_perfil)) {
                        ?>  
                              <img src="uploads_foto_perfil/<?php echo $row_use_menu['img_perfil'] ?>" style="width:50px;height:50px;" class="img-circle" alt="" />
                            
                        <?php    
                            } else {
                                echo '<img src="img/img_sem.jpg" class="img-circle" style="width:50px;height:50px;" alt="" >';
                            }

                        ?>                            
                        </div>
                        <div class="info">
                            <p>Olá, <?php echo ucfirst($nome[0]); ?></p>
                            <small style="color: #eee;"><?php echo $saudacao; ?></small>
                        </div>
                    </div>
                </div>
                <!-- fim: foto + boas vindas -->

            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <!-- Perfil de Administrativo -->
                <?php if ($cd_perfil == 1 || $cd_perfil == 2): ?>
                <li class="treeview menu_home_left" >
                    <a href="home.php" onclick="window.location.replace('home.php')">
                        <i class="fa fa-dashboard"></i> <span>Home</span>
                    </a>
                </li>
            <?php endif ?>
            
            <!-- Perfil de Administrativo -->
            <?php if ($cd_perfil == 1 || $cd_perfil == 2): ?>
            <li class="treeview">
                <a href="#" class="a_menu_agenda_left">
                    <i class="fa fa-calendar"></i>
                    <span>Agenda</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="lista_agenda.php"><i class="fa fa-circle-o"></i> Agenda de Exames</a></li>
                </ul>
            </li>
        <?php endif ?>

        <!-- Perfil de Administrativo -->
        <?php if ($cd_perfil == 1 || $cd_perfil == 3 || $cd_perfil == 2): ?>
        <li class="treeview">
            <a href="#" class="a_menu_solic_left">
                <i class="fa fa-medkit"></i>
                <span>Solicitações</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="cad_solicitacao.php"><i class="fa fa-circle-o"></i> Nova Solicitação</a></li>
                <li><a href="lisita_solic_exames.php"><i class="fa fa-circle-o"></i> Situação</a></li>
            </ul>
        </li>
    <?php endif ?>

    <!-- Perfil de Administrativo -->
    <?php if ($cd_perfil == 1 || $cd_perfil == 2): ?>
    <li class="treeview">
        <a href="#" class="a_menu_user_left">
            <i class="fa fa-user"></i>
            <span>Usuário</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="lista_usuarios.php"><i class="fa fa-circle-o"></i> Usuários</a></li>
            <li><a href="cad_usuario.php"><i class="fa fa-circle-o"></i> Novo Usuário</a></li>
        </ul>
    </li>
<?php endif ?>

<!-- Perfil de Administrativo -->
<?php if ($cd_perfil == 1 || $cd_perfil == 2): ?>
    <li class="treeview menu_medico_left">
        <a href="#" class="a_menu_medico_left">
            <i class="fa fa-stethoscope"></i>
            <span>Médico</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="lista_medicos.php"><i class="fa fa-circle-o"></i> Médicos</a></li>
            <li><a href="cad_medico.php"><i class="fa fa-circle-o"></i> Novo Médico</a></li>                        
        </ul>
    </li>
<?php endif ?>

<!-- Perfil de Administrativo -->
<?php if ($cd_perfil == 1 || $cd_perfil == 2): ?>
    <li class="treeview menu_exame_left">
        <a href="#" class="a_menu_exame_left">
            <i class="fa fa-plus-square"></i>
            <span>Exame</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="lista_exames.php"><i class="fa fa-circle-o"></i> Exames </a></li>
            <li><a href="cad_exame.php"><i class="fa fa-circle-o"></i> Novo Exame</a></li>
        </ul>
    </li>
<?php endif ?>

<!-- Perfil de Administrativo -->
<?php if ($cd_perfil == 1 || $cd_perfil == 2): ?>
    <li class="treeview menu_exame_left">
        <a href="#" class="a_menu_exame_left">
            <i class="fa fa-file-text-o"></i>
            <span>Relatórios</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="r_agenda_medico.php"><i class="fa fa-circle-o"></i> Agenda x Médico </a></li>
        </ul>
    </li>
<?php endif ?>

</ul>
</section>
</aside>
