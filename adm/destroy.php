<?php ob_start();
session_start();
session_unset($_SESSION['usr_cd_user']);
session_unset($_SESSION['usr_nome']);
session_unset($_SESSION['usr_perfil']);	
session_destroy();
header('Location: index.php');
?>
<?php	
ob_end_flush();
?>