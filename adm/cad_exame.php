<?php include 'head.php'; ?>
<body class="cl-default fixed">

    <!-- inicio:navbar top -->
    <?php include 'head_menu_top.php'; ?>
    <!-- Fin:navbar top -->
    

    <!-- inicio: Toda Lateral do menu -->
    <?php include 'head_menu_left.php'; ?>
    <!-- fim: Toda Lateral do menu -->

    <!--lib. http://plentz.github.io/jquery-maskmoney/ -->
    <script src="js/jquery.maskMoney.min.js"></script>

    <script src="js/script-cadastrar-exame.js"></script>

    <style>
    .form-group{
        padding-left:10px;
        padding-right: 10px;
    }
    </style>

    <aside class="right-side">
        <section class="content">
            <h1>
                Cadastro Exame
            </h1>
            <!-- start:breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-plus-square"></i> Exame</a></li>
                <li class="active">Novo Exame</li>
            </ol>
            <!-- end:breadcrumb -->

            <!-- start:content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box blank-page">


                        <form class="form-horizontal tasi-form" id="form_exame" method="post"
                              accept-charset="UTF-8" enctype="application/x-www-form-urlencoded"
                              autocomplete="off" >

                            <input type="hidden" name="controle" id="controle" value="N">
                            <fieldset>
                                <!-- Form Name -->
                                <legend>Exame</legend>

                                <div class="row">
                                    <div class="col-md-10">

                                      <section class="panel">

                                        <div class="panel-body">
                                            <form class="form-horizontal" role="form">

                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" class="form-control" id="nome" name="nome" maxlength="100" required placeholder="Digite o nome do exame">
                                                </div>

                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="valor">Valor R$</label>
                                                        <input type="text" class="form-control" data-thousands="." maxlength="20" data-decimal="," required id="valor" name="valor" placeholder="Digite o valor do exame">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="valor">% Rateio Médico</label>
                                                        <input type="number" class="form-control"  maxlength="20"  required id="rateio" name="rateio" placeholder="Digite o valor de rateio">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="descricao">Descrição</label>
                                                <textarea class="form-control" id="descricao" name="descricao" maxlength="500" required  rows="5"></textarea>
                                            </div>

                                            <br>

                                            <div align="right">
                                                <button class="btn btn-danger btn_reset">Cancelar</button>
                                                <button type="submit" class="btn btn-success">Salvar</button>
                                            </div>


                                        </form>

                                    </div>
                                </section>


                            </div>
                        </div>

                    </fieldset>
                </form>





            </div>
        </div>
    </div>
    <!-- end:content -->

</section>
</aside>
<!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->


    <!-- Modal -->
    <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h4>
                </div>
                <div class="modal-body" align="center">
                    <h1 id="msg_modal" style="color: #2ECC71;">   </h1>
                </div>
                <div class="modal-footer"  align="right">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>

                </div>
            </div>
        </div>
    </div>
    <!-- end modal  -->



</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Oct 2015 00:45:02 GMT -->
</html>