<?php include 'head.php'; ?>
<?php include 'conexao/config.php' ?>

<body class="cl-default fixed">

    <link href="plugins/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />

    <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="css/table-responsive.css">

    <script src="js/script-controle_agenda.js"></script>

    <style>

    .warning {
        background-color: #F9EBA2 !important;
    }

    tr.warning > td {
        background-color: #F9EBA2 !important;
    }

    .danger{
        background-color: #F69E9E !important;
    }

    tr.danger > td {
        background-color: #F69E9E !important;
    }

    .testTable {
      display: table;
      margin: 0px;
      padding: 0px;
  }

  .testRow {
      display: table-row;
  }

  .testRow > span {
      list-style:none;
      display: table-cell;
      padding: 2px 6px;
  }

  .testHeader {
      display: table-header-group;
      /*position: absolute;*/
  }

  .testHeader span {
  }

  .testBody {
      display: table-row-group;
  }

  </style>


  <!-- inicio:navbar top -->
  <?php include 'head_menu_top.php'; ?>
  <!-- Fin:navbar top -->

  <!-- inicio: Toda Lateral do menu -parametro($vMenu) -->
  <?php include 'head_menu_left.php'; ?>
  <!--  Toda Lateral do menu -->


  <script src="plugins/data-tables/dataModificado/jquery.dataTables.js"></script>
  <script src="plugins/data-tables/dataModificado/dataTables.bootstrap.js"></script>


  <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {

    $('.medico_vazio').hide();

    $('#lista_agenda_table').dataTable( {
        "pageLength": 10,
        "lengthChange": false,  
                        "order": [[ 3, "desc" ]], //ordena por coluna 
                        "language": {
                            "url": "plugins/data-tables/dataModificado/Portuguese-Brasil.json" //tradução para português
                        },
                        stateSave: true, //salvar pesquisa em tempo 
                        "searching": true //oculta ou mostra
                    }); 




    $('.solic_confirme').click(function(){

        var cod = $(this).attr('data-cod');

        if (cod!='' && cod!=null && cod!=undefined ) {

            $.ajax({
              method: "POST",
              url: "model/grud_solicitacao.php",
              data: { cd_solicitacao: cod },
              success: function( data ) {
                var msg = 'Deseja confirmar esse agendamento? <br/>';
                $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                $('#controle_confirma').val('A');
                $('#cod_confirmado').val(cod);
                $('#modalConfirma').modal('show');
            },
            error: function (){
                $('.btn-controle').hide();  
                $('.btn-fechar').show();                  
                $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                $('#modalConfirma').modal('show');
            }
        });    
        }

    });


    $('.solic_cancelar').click(function(){

        var cod = $(this).attr('data-cod');

        if (cod!='' && cod!=null && cod!=undefined ) {

            $.ajax({
              method: "POST",
              url: "model/grud_solicitacao.php",
              data: { cd_solicitacao: cod },
              success: function( data ) {
                var msg = 'Deseja cancelar esse agendamento? <br/>';
                $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                $('#controle_confirma').val('I');
                $('#cod_confirmado').val(cod);
                $('#modalConfirma').modal('show');
            },
            error: function (){
                $('.btn-controle').hide();  
                $('.btn-fechar').show();                  
                $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                $('#modalConfirma').modal('show');
            }
        });    
        }

    });



    $('.solic_excluir').click(function(){

        var cod = $(this).attr('data-cod');

        if (cod!='' && cod!=null && cod!=undefined ) {

            $.ajax({
              method: "POST",
              url: "model/grud_solicitacao.php",
              data: { cd_solicitacao: cod },
              success: function( data ) {
                var msg = 'Deseja excluir esse agendamento? <br/>';
                $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                $('#controle_confirma').val('E');
                $('#cod_confirmado').val(cod);
                $('#modalConfirma').modal('show');
            },
            error: function (){
                $('.btn-controle').hide();  
                $('.btn-fechar').show();                  
                $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                $('#modalConfirma').modal('show');
            }
        });    
        }

    });


    $('.solic_pendente').click(function(){

        var cod = $(this).attr('data-cod');

        if (cod!='' && cod!=null && cod!=undefined ) {

            $.ajax({
              method: "POST",
              url: "model/grud_solicitacao.php",
              data: { cd_solicitacao: cod },
              success: function( data ) {
                var msg = 'Deseja alterar o status do agendamento para pendente? <br/>';
                $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                $('#controle_confirma').val('AC');
                $('#cod_confirmado').val(cod);
                $('#modalConfirma').modal('show');
            },
            error: function (){
                $('.btn-controle').hide();  
                $('.btn-fechar').show();                  
                $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                $('#modalConfirma').modal('show');
            }
        });    
        }

    });




$('.btn-confirma').click(function(){
    var cod = $('#cod_confirmado').val();
    var controle = $('#controle_confirma').val();

    if (cod!='' && cod!=null && cod!=undefined ) {
        console.log('btn-confirma');
        console.log(cod +' - '+controle );

        $.ajax({
            method: "POST",
            url: "model/grud_solicitacao.php",
            data: { cd_solicitacao: cod, controle: controle },
            success: function( data ) {

                $('#msg_modal').css( "color", "#000000" ).html(data);
                $('.btn-controle').hide();
                $('.btn-fechar').show();
            },
            error: function (){
                $('.btn-controle').hide();
                $('.btn-fechar').show();
                $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                $('#modalConfirma').modal('show');
            }
        });
    }

});

$('#modalConfirma').on('hidden.bs.modal', function (e) {

    $('#cod_confirmado').val('');
    $('#controle_confirma').val('');
    $('.btn-controle').show();
    $('.btn-fechar').hide();
    $('#msg_modal').css( "color", "#000000" ).html('');
    window.location.reload();
})


} );

function cadMedicoExec(cod){

    var fomrExec = $('#cadMed'+cod).serialize()

    var cd_medico = $("#medico"+cod+" option:selected").val();

    if (cd_medico == null || cd_medico == '') {
        $('#medico_vazio'+cod).show();
    }else{
        $('#medico_vazio'+cod).hide();

        $.ajax({
            method: "POST",
            url: "model/grud_solicitacao.php",
            data:  fomrExec ,
            success: function( data ) {

                $('#msg_modal').css( "color", "#000000" ).html(data);
                $('.btn-controle').hide();
                $('.btn-fechar').show();

                $('.btn-controle').show();
                $('.btn-fechar').hide();
                $('#msg_modal').css( "color", "#000000" ).html('');
                window.location.reload();
            },
            error: function (){
                console.log('erro')
                $('.btn-controle').hide();
                $('.btn-fechar').show();
                $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                $('#modalConfirma').modal('show');
            }
        });

    };
}


function PrintElem(elem){
    Popup($(elem).html());
}
function Popup(data){

    var mywindow = window.open('', 'my div', 'height=400,width=600');

    mywindow.document.write('<html><head><title>Agenda de Exames</title>');
    /*optional stylesheet*/ 
      //  mywindow.document.write('<link rel="stylesheet" href="css/imprimeAgenda.css" type="text/css" />');
      mywindow.document.write('</head><body >');
      mywindow.document.write('<br><br>');
      mywindow.document.write(data);
      mywindow.document.write('</body></html>');
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        mywindow.close();
        return true;
    }


    </script>


    <?php 

    $query = "SELECT * FROM solicitacao order by cd_solicitacao desc ";
    $result = mysqli_query($conn, $query);
    $total_num_rows = mysqli_num_rows($result);



    function situacao($args){
        switch ($args) {
            case 'A':
            $situacao =  '<span class="label label-success">Confirmado</span>';
            break;
            case 'I':
            $situacao = '<span class="label label-warning">Cancelado</span>';
            break;
            case 'E':
            $situacao = '<span class="label label-danger">Excluído</span>';
            break;
            case 'AC':
            $situacao = '<span class="label label-info">Pendente</span>';
            break;
            default:
            $situacao =  "";
        }

        echo $situacao;
    }

    function linhaStatus ($args){
        switch ($args) {
            case 'A':
            $class =  '';
            break;
            case 'I':
            $class = ' class="warning" ';
            break;
            case 'E':
            $class = ' class="danger" ';
            break;
            default:
            $class =  '';
        }

        echo $class;
    }

    function retornaExameDetalhe($cd_exame, $conn){
        $query = "SELECT * FROM exame where cd_exame = '$cd_exame'";
        $result = mysqli_query($conn, $query);
        $row = mysqli_fetch_array($result);


        $valorRatio =  ( $row['rateio'] / 100 ) * $row['valor'];
        $valorRatioX  = number_format($valorRatio, 2, ',', '.');
        $valorX  = number_format($row['valor'], 2, ',', '.');


        echo '<span class="label label-success">R$ '.$valorX.'</span>' .
        '<span>'. utf8_encode($row['nome']) . '</span> ' .
        '<span class="label label-info">'.$row['rateio'].'% - R$ '.$valorRatioX.'</span>';
    }

    function medicoExecutante($conn, $elem){

        if ($elem == null || $elem ==  '' || $elem ==  0) {
          echo "<small>Por favor, informa o médico executante no menu de configurações</small>";
      }else{
        $queryMed = "SELECT * FROM medico where cd_medico = '$elem' ";
        $resultMed = mysqli_query($conn, $queryMed);
        $row = mysqli_fetch_array($resultMed);
        echo $row['nome'];
    }
}

?>

<aside class="right-side">
    <section class="content">
        <h1>
            Agenda de Exames         
        </h1>
        <!-- start:breadcrumb -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-calendar"></i> Agenda</a></li>
            <li class="active">Agenda</li>
        </ol>
        <!-- end:breadcrumb -->

        <!-- start:content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box blank-page">
                    <div class="row">
                        <div class="col-md-12">
                            <section id="unseen">
                                <table  class="display table table-bordered table-striped table-condensed" id="lista_agenda_table">
                                    <thead>
                                        <tr>
                                            <th width="5%">Agenda</th>
                                            <th width="15%">Data - Hora</th>
                                            <th width="30%">Paciente</th>
                                            <th width="30%">Méd. Solicitante</th>                                            
                                            <th width="30%">Méd. Executante</th>
                                            <th width="15%">Situação</th>
                                            <th width="15%">Configurações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $cont = 1;
                                        while($row = mysqli_fetch_array($result)){ ?>
                                        <tr>
                                            <td align="center">
                                                <button type="button" class="btn btn-info btn-rounded"  data-toggle="modal" data-target="#myModal<?php echo $cont;?>">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </td>
                                            <td><?php echo $row['dt_solicitacao']; ?></td>
                                            <td><?php echo $row['nm_paciente']; ?></td>
                                            <td><?php medicoExecutante($conn, $row['cd_medico_solicitante']); ?></td>
                                            <td><?php medicoExecutante($conn,$row['cd_medico_executante']); ?></td>
                                            <td align="center"><?php  situacao($row['status']); ?></td>
                                            <td align="center">

                                                <!-- opções button -->
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Opções
                                                        <i class="fa fa-cog"></i>
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" style="text-align: left;    margin-left: -76px;">

                                                        <li data-cod=""  class="solic_confirme"><a href="#"  data-toggle="modal" data-target="#medicoExec<?php echo $cont;?>"> <i class="fa fa-user-md"></i> Médico Executante </a></li>
                                                        <li class="divider"></li>
                                                        <li data-cod="<?php echo $row['cd_solicitacao']; ?>"  class="solic_confirme"><a href="#"> <i class="glyphicon glyphicon-ok icon-circle icon-success"></i> Confirmar </a></li>
                                                        <li data-cod="<?php echo $row['cd_solicitacao']; ?>"  class="solic_pendente"><a href="#"> <i class="fa fa-warning icon-circle icon-info"></i> Pendente </a></li>
                                                        <li data-cod="<?php echo $row['cd_solicitacao']; ?>"  class="solic_cancelar"><a href="#"> <i class="fa fa-ban icon-circle icon-warning"></i> Cancelar </a></li>                                                     
                                                        <li class="divider"></li>
                                                        <li data-cod="<?php echo $row['cd_solicitacao']; ?>"  class="solic_excluir"><a href="#"> <i class="fa  fa-trash-o icon-circle icon-danger"></i> Excluir </a></li>
                                                    </ul>
                                                </div>
                                                <!-- end opções button -->

                                            </td>

                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal<?php echo $cont;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Visualizar Agendamento</h4>
                                                </div>
                                                <div class="modal-body" id="imprime<?php echo $cont;?>">
                                                   <ul class="list-group">
                                                    <li class="list-group-item"><b>Data - Hora : </b><?php echo $row['dt_solicitacao']; ?></li>
                                                    <li class="list-group-item"><b>Paciente :    </b><?php echo $row['nm_paciente']; ?></li>
                                                    <li class="list-group-item"><b>CPF :         </b><?php echo $row['cpf_paciente']; ?></li>
                                                    <li class="list-group-item"><b>Medico Solicitante :      </b><?php echo $row['mn_medico']; ?></li>
                                                    <li class="list-group-item"><b>Medico Executante :      </b><?php echo $row['mn_medico']; ?></li>
                                                    <li class="list-group-item"><b>Telefone :    </b><?php echo $row['telefone']; ?></li>
                                                    <li class="list-group-item"><b>Celular :     </b><?php echo $row['celular']; ?></li>
                                                    <li class="list-group-item">
                                                        <?php $list_exames = explode("|", $row['exames']); ?>
                                                        <ul class="testTable">
                                                            <div class="testHeader">
                                                                <li class="testRow">
                                                                    <span><b>Valor</b></span>
                                                                    <span><b>Exames</b></span>
                                                                    <span><b>Rateio</b></span>
                                                                </li>
                                                            </div>
                                                            <div class="testBody">
                                                                <?php for ($i=0; $i < sizeof($list_exames); $i++) {?>
                                                                <li class="testRow">
                                                                    <?php retornaExameDetalhe($list_exames[$i], $conn); ?>
                                                                </l1>
                                                                <?php } ?>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="list-group-item"><b>Descrição :   </b><?php echo $row['descricao']; ?></li>
                                                    <li class="list-group-item"><b>Situação :    </b><?php  situacao($row['status']); ?></li>
                                                </ul>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" align="left" onclick="PrintElem('#imprime<?php echo $cont;?>')">Imprimir</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- modal medico executante -->
                                <div class="modal fade" id="medicoExec<?php echo $cont;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document" style="width: 425px;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Informa Médico Executante</h4>
                                            </div>
                                            <div class="modal-body" style="padding-bottom: 5px;">
                                                <div class="form-group" id="listaMedico">

                                                    <form  method="POST" accept-charset="utf-8" id="cadMed<?php echo $cont;?>">                                                       

                                                      <label for="estado">Médico Executante</label>
                                                      <select class="form-control m-bot15" id="medico<?php echo $cont;?>" name="cd_medico_executante"  required>
                                                        <option value="">Selecione o médico</option> 
                                                        <?php 
                                                        $queryMed = "SELECT * FROM medico where status = 'A' order by nome";
                                                        $resultMed = mysqli_query($conn, $queryMed);
                                                        while($rowMed = mysqli_fetch_array($resultMed)){ ?>                                                       
                                                        <option value="<?php echo $rowMed['cd_medico']; ?>">
                                                            <?php echo $rowMed['nome']; ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                    <input type="hidden" name="controle" value="med">
                                                    <input type="hidden" name="cd_solicitacao" value="<?php echo $row['cd_solicitacao']; ?>">
                                                </form>
                                                <small style="color: red;" class="medico_vazio" id="medico_vazio<?php echo $cont;?>">
                                                    Por favor, informa o médico executante.
                                                </small>
                                            </div>
                                        </div>  
                                        <div class="modal-footer"  align="right">
                                            <button type="button" class="btn btn-danger btn-controle" data-dismiss="modal">Cancelar</button>
                                            <button type="button" class="btn btn-primary btn-confirma btn-controle" onclick="cadMedicoExec(<?php echo $cont;?>)">Confirmar</button>
                                            <button type="button" class="btn btn-danger btn-fechar" style="display:none;"  data-dismiss="modal">Fechar</button>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end modal medico executante -->

                        </tr> 
                        <?php $cont++; } ?>                                     
                    </tbody>
                </table>
            </section>
        </div>                            
    </div>





</div>
</div>
</div>
<!-- end:content -->

</section>
</aside>
<!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->


<!-- modal confirmação -->
<div class="modal fade" id="modalConfirma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h3>
            </div>
            <div class="modal-body" align="center">
                <input type="hidden" id="cod_confirmado" value="" >
                <input type="hidden" id="controle_confirma" value="" >
                <h2 id="msg_modal" style="color: #000000;">   </h2>
            </div>
            <div class="modal-footer"  align="right">
                <button type="button" class="btn btn-primary btn-confirma btn-controle" >Sim</button>
                <button type="button" class="btn btn-danger btn-controle" data-dismiss="modal">Não</button>
                <button type="button" class="btn btn-danger btn-fechar" style="display:none;"  data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>

<!-- end modal confirmação -->

i



</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Oct 2015 00:45:02 GMT -->
</html>