<?php include 'head.php'; ?>
<?php include 'conexao/config.php' ?>

<?php

$query = "SELECT * FROM exame order by cd_exame desc";
$result = mysqli_query($conn, $query);
$total_num_rows = mysqli_num_rows($result);

function situacao($args){
    switch ($args) {
        case 'A':
        $situacao =  '<span class="label label-success">Ativo';
        break;
        case 'I':
        $situacao = '<span class="label label-info">Inativo</span>';
        break;
        case 'E':
        $situacao = '<span class="label label-danger">Excluído</span>';
        break;
    }

    echo $situacao;
}

function linhaStatus ($args){
    switch ($args) {
        case 'A':
            $class =  '';
            break;
        case 'I':
            $class = ' class="warning" ';
            break;
        case 'E':
            $class = ' class="danger" ';
            break;
        default:
            $class =  '';
    }

    echo $class;
}


?>
<body class="cl-default fixed">

    <link href="plugins/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />

    <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="css/table-responsive.css">

    <style>

        .warning {
            background-color: #F9EBA2 !important;
        }

        tr.warning > td {
            background-color: #F9EBA2 !important;
        }

        .danger{
            background-color: #F69E9E !important;
        }

        tr.danger > td {
            background-color: #F69E9E !important;
        }

    </style>




    <!-- inicio:navbar top -->
    <?php include 'head_menu_top.php'; ?>
    <!-- Fin:navbar top -->
    
    <!-- inicio: Toda Lateral do menu -parametro($vMenu) -->
    <?php include 'head_menu_left.php'; ?>
    <!--  Toda Lateral do menu -->


    <script src="plugins/data-tables/dataModificado/jquery.dataTables.js"></script>
    <script src="plugins/data-tables/dataModificado/dataTables.bootstrap.js"></script>


    <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {

        $('.a_menu_exame_left').click();

        $('#lista_medicos').dataTable( {
            "pageLength": 10,
            "lengthChange": false,  
                        "order": [[ 3, "desc" ]], //ordena por coluna 
                        "language": {
                            "url": "plugins/data-tables/dataModificado/Portuguese-Brasil.json" //tradução para português
                        },
                        stateSave: true, //salvar pesquisa em tempo 
                        "searching": true //oculta ou mostra
        });


        //alterar exame
        $('.aletar_ex').click(function(){
            var cod = $(this).attr('data-cod');
            var form = $('#form_enviar');

            if (cod!='' && cod!=null && cod!=undefined ) {

                $('#controle').val('M');
                $('#cod_exame').val(cod);
                form.attr('action', 'alterar_exame.php');
                form.submit();
            }
        });


        //inativar exame
        $('.inativar_ex').click(function(){
            var cod = $(this).attr('data-cod');

            if (cod!='' && cod!=null && cod!=undefined ) {

                $.ajax({
                    method: "POST",
                    url: "model/consulta_nome_exame.php",
                    data: { cod_exame: cod },
                    success: function( data ) {
                        var msg = 'Deseja inativar este exame? <br/>';
                        $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                        $('#controle_confirma').val('I');
                        $('#cod_confirmado').val(cod);
                        $('#modalConfirma').modal('show');
                    },
                    error: function (){
                        $('.btn-controle').hide();
                        $('.btn-fechar').show();
                        $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                        $('#modalConfirma').modal('show');
                    }
                });
            }

        });


        //ativar exame
        $('.ativar_ex').click(function(){
            var cod = $(this).attr('data-cod');

            if (cod!='' && cod!=null && cod!=undefined ) {

                $.ajax({
                    method: "POST",
                    url: "model/consulta_nome_exame.php",
                    data: { cod_exame: cod },
                    success: function( data ) {
                        var msg = 'Deseja Ativar o exame? <br/>';
                        $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                        $('#controle_confirma').val('A');
                        $('#cod_confirmado').val(cod);
                        $('#modalConfirma').modal('show');
                    },
                    error: function (){
                        $('.btn-controle').hide();
                        $('.btn-fechar').show();
                        $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                        $('#modalConfirma').modal('show');
                    }
                });
            }

        });


        $('.btn-confirma').click(function(){
            var cod = $('#cod_confirmado').val();
            var controle = $('#controle_confirma').val();

            if (cod!='' && cod!=null && cod!=undefined ) {
                console.log('btn-confirma');
                console.log(cod +' - '+controle );

                $.ajax({
                    method: "POST",
                    url: "model/grud_exame.php",
                    data: { cod_exame: cod, controle: controle },
                    success: function( data ) {

                        $('#msg_modal').css( "color", "#000000" ).html(data);
                        $('.btn-controle').hide();
                        $('.btn-fechar').show();
                    },
                    error: function (){
                        $('.btn-controle').hide();
                        $('.btn-fechar').show();
                        $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                        $('#modalConfirma').modal('show');
                    }
                });
            }

        });


        //excluir medico
        $('.excluir_ex').click(function(){
            var cod = $(this).attr('data-cod');
            console.log('excluir_med');

            if (cod!='' && cod!=null && cod!=undefined ) {

                $.ajax({
                    method: "POST",
                    url: "model/consulta_nome_exame.php",
                    data: { cod_exame: cod },
                    success: function( data ) {
                        var msg = 'Deseja excluir este exame? <br/>';
                        $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                        $('#controle_confirma').val('E');
                        $('#cod_confirmado').val(cod);
                        $('#modalConfirma').modal('show');
                    },
                    error: function (){
                        $('.btn-controle').hide();
                        $('.btn-fechar').show();
                        $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                        $('#modalConfirma').modal('show');
                    }
                });
            }

        });






        $('#modalConfirma').on('hidden.bs.modal', function (e) {

            $('#cod_confirmado').val('');
            $('#controle_confirma').val('');
            $('.btn-controle').show();
            $('.btn-fechar').hide();
            $('#msg_modal').css( "color", "#000000" ).html('');
            window.location.reload();
        })


    } );
    </script>

<?php 

function porcentagem_xn ( $porcentagem, $total ) {
    $valor =  ( $porcentagem / 100 ) * $total;
    $valorx  = number_format($valor, 2, ',', '.');
    echo 'R$ ' . $valorx;
}
 ?>
    <form id="form_enviar" method="post" accept-charset="UTF-8"  >
           <input type="hidden"  id="controle" name="controle"  value="">               
           <input type="hidden"  id="cod_exame" name="cod_exame"  value="">
    </form>


       


    <aside class="right-side">
        <section class="content">
            <h1>
                Lista de Exames               
            </h1>
            <!-- start:breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-plus-square"></i> Exames</a></li>
                <li class="active">Lista de exames</li>
            </ol>
            <!-- end:breadcrumb -->

            <!-- start:content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box blank-page">
                        <div class="row">
                            <div class="col-md-12">
                                <section id="unseen">
                                    <table  class="display table table-bordered table-striped table-condensed" id="lista_medicos">
                                        <thead>
                                            <tr>
                                                <th width="40%">Nome</th>
                                                <th width="20%">Valor</th>
                                                <th width="20%">Rateio Médico</th>
                                                <th width="10%">Situação</th>
                                                <th width="20%">Configuração</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php while($row = mysqli_fetch_array($result)){ ?>
                                            <tr>
                                                <td><?php echo utf8_encode($row['nome']); ?></td>
                                                <td><?php
                                                  $valor = utf8_encode($row['valor']);
                                                  echo 'R$ '.$valorX = number_format($valor, 2, ',', '.'); ?>
                                                 </td>
                                                 <td><?php
                                                  $rateio = utf8_encode($row['rateio']);
                                                  echo $rateio . '% - ';
                                                  porcentagem_xn ( $rateio, $valor ); 
                                                  ?>
                                                 </td>
                                                <td align="center">
                                                    <?php  situacao($row['status']); ?>
                                                </td>
                                                <td align="center">

                                                    <!-- opções button -->
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                            Opções
                                                            <i class="fa fa-cog"></i>
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" style="text-align: left;">
                                                            <li data-cod="<?php echo $row['cd_exame']; ?>" class="aletar_ex"><a href="#"> <i class="fa fa-edit icon-circle icon-info"></i> Alterar </a></li>
                                                            <li data-cod="<?php echo $row['cd_exame']; ?>"  class="inativar_ex"><a href="#"> <i class="fa fa-ban icon-circle icon-warning"></i> Inativar </a></li>
                                                            <li data-cod="<?php echo $row['cd_exame']; ?>"  class="ativar_ex"><a href="#"> <i class="glyphicon glyphicon-ok icon-circle icon-success"></i> Ativar </a></li>
                                                            <li class="divider"></li>
                                                            <li data-cod="<?php echo $row['cd_exame']; ?>" class="excluir_ex"><a href="#"> <i class="fa  fa-trash-o icon-circle icon-danger"></i> Excluir </a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- end opções button -->

                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </section>
                            </div>                            
                        </div>





                    </div>
                </div>
            </div>
            <!-- end:content -->

        </section>
    </aside>
    <!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->



    <!-- modal confirmação -->
    <div class="modal fade" id="modalConfirma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h4>
                </div>
                <div class="modal-body" align="center">
                    <input type="hidden" id="cod_confirmado" value="" >
                    <input type="hidden" id="controle_confirma" value="" >
                    <h2 id="msg_modal" style="color: #000000;">   </h2>
                </div>
                <div class="modal-footer"  align="right">
                    <button type="button" class="btn btn-primary btn-confirma btn-controle" >Sim</button>
                    <button type="button" class="btn btn-danger btn-controle" data-dismiss="modal">Não</button>
                    <button type="button" class="btn btn-danger btn-fechar" style="display:none;"  data-dismiss="modal">Fechar</button>

                </div>
            </div>
        </div>
    </div>

    <!-- end modal confirmação -->

</body>
</html>