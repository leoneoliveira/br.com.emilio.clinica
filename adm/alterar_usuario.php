<?php include 'head.php'; ?>
<?php include 'conexao/config.php'; ?>
<body class="cl-default fixed">

<!-- inicio:navbar top -->
<?php include 'head_menu_top.php'; ?>
<!-- Fin:navbar top -->

<!-- inicio: Toda Lateral do menu -->
<?php include 'head_menu_left.php'; ?>
<!-- fim: Toda Lateral do menu -->

<?php


$queryM = "SELECT * FROM medico WHERE status = 'A'";
$resultM = mysqli_query($conn, $queryM);
$total_num_rows = mysqli_num_rows($resultM);






if (isset($_POST['cd_user'])) {
    $id =  $_POST['cd_user'];
}else{
    if (isset($_GET['cd_user'])) {
      $id =  $_GET['cd_user'];
    }else{
      $id = '';  
  }    
}

$query = "SELECT * FROM user WHERE cd_user = '$id'";
$result = mysqli_query($conn, $query);

$result = mysqli_query($conn, $query);
$total_num_rows = mysqli_num_rows($result);

$row = mysqli_fetch_array($result);

echo $vinculo_user = $row['cd_medico'];

$status_bloqueio = false;
if($vinculo_user !='' AND $vinculo_user !=null){
    $status_bloqueio = true;

    $cod_medico = $vinculo_user;
    $queryMedico = "SELECT * FROM medico WHERE cd_medico = '$cod_medico'";
    $resultMedico = mysqli_query($conn, $queryMedico);

    if (!$resultMedico) {
        echo 'Não foi possível executar a consulta: ' . mysql_error();
        exit;
    }
    $rowMed = mysqli_fetch_array($resultMedico);

    $nome = utf8_encode($rowMed['nome']);
    $email = utf8_encode($rowMed['email']);
    $telefone =  utf8_encode($rowMed['telefone']);
    $celular = utf8_encode($rowMed['celular']);
    $endereco = utf8_encode($rowMed['endereco']);
    $cidade = utf8_encode($rowMed['cidade']);
    $bairro = utf8_encode($rowMed['bairro']);
    $cep = utf8_encode($rowMed['cep']);
    $estado = utf8_encode($rowMed['estado']);

} else {
    $status_bloqueio = false;

    $nome = utf8_encode($row['nome']);
    $email = utf8_encode($row['email']);
    $telefone =  utf8_encode($row['telefone']);
    $celular = utf8_encode($row['celular']);
    $endereco = utf8_encode($row['endereco']);
    $cidade = utf8_encode($row['cidade']);
    $bairro = utf8_encode($row['bairro']);
    $cep = utf8_encode($row['cep']);
    $estado = utf8_encode($row['estado']);

}


if($row['img_perfil']!='' AND $row['img_perfil']!=null){

    $url_img = "uploads_foto_perfil/".utf8_encode($row['img_perfil']);
    if (file_exists($url_img)) {
        $url_img = "uploads_foto_perfil/".utf8_encode($row['img_perfil']);
    } else {
        $url_img = "img/user/avatar-".(rand(1,5)).".jpg";
    }

} else {
    $url_img = "img/user/avatar-".(rand(1,5)).".jpg";
}


?>

<style>
    .form-group{
        padding-left:10px;
        padding-right: 10px;
    }
</style>
<script>
    var url_img_default = '<?php echo $url_img; ?>';
    var form_bloqueio = '<?php echo $status_bloqueio;?>';

</script>
<script src="js/script-alterar-usuario.js"></script>


<link rel="stylesheet" href="plugins/file-uploader/css/jquery.fileupload.css">



<aside class="right-side">
    <section class="content">
        <h1>
            Alterar Cadastro Usuário
        </h1>
        <!-- start:breadcrumb -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Usuário</a></li>
            <li class="active">Alterar Usuário</li>
        </ol>
        <!-- end:breadcrumb -->

        <!-- start:content -->
        <div class="row">
            <div class="col-md-12">
                <div class="box blank-page">

                    <!-- Controle de pesquisa de medico -->
                    <div id="c_medico"></div>


                    <fieldset>
                        <!-- Form Name -->
                        <legend>Usuário</legend>

                        <div class="row">
                            <div class="col-md-10">

                                <section class="panel">

                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" id="form_cad_usr" method="post"  enctype="multipart/form-data">
                                            <input type="hidden" name="controle" value="M">
                                            <input type="hidden" name="id" value="<?php echo $row['cd_user'];?>">

                                            <div class="row">
                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="vinculo_user">Vinculo Médico</label>
                                                        <div class="col-md-4">
                                                            <div class="radio">
                                                                <label for="vinculo_user-1">
                                                                    <input type="radio" name="vinculo_user" id="vinculo_user-1" value="1" <?php echo ($vinculo_user != '' AND $vinculo_user != null) ? ' checked ' : ' '; ?> >
                                                                    Sim
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label for="vinculo_user-2">
                                                                    <input type="radio" name="vinculo_user" id="vinculo_user-2" value="2" <?php echo ($vinculo_user == '' OR $vinculo_user== null) ? ' checked ' : ' '; ?>  >
                                                                    Não
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6" >
                                                    <div class="form-group" id="listaMedico">
                                                        <label for="estado">Médico</label>
                                                        <select class="form-control m-bot15" id="vinculoMedico" name="id_medico"  required>
                                                            <option value="">Selecione o médico</option>
                                                            <?php while($rowM = mysqli_fetch_array($resultM)){ ?>
                                                                <option value="<?php echo utf8_encode($rowM['cd_medico']); ?>"  <?php echo ($rowM['cd_medico']==$vinculo_user)? ' selected ' :' ' ?>   >
                                                                    <?php echo utf8_encode($rowM['nome']); ?>
                                                                </option  >
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nome</label>
                                                <input type="text" class="form-control" id="nome_user" name="nome_user" placeholder="Digite o nome" value="<?php echo $nome; ?>" required>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Login</label>
                                                        <input type="text" class="form-control" id="login_user" name="login_user" placeholder="Digite o login" value="<?php echo utf8_encode($row['login']); ?>" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Senha</label>
                                                        <input type="password" class="form-control" id="senha_user" name="senha_user" value="<?php echo utf8_encode($row['senha']); ?>"  placeholder="Digite a senha"  required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Confirme Senha</label>
                                                        <input type="password" class="form-control" id="confirme_senha_user" name="confirme_senha_user" value="<?php echo utf8_encode($row['senha']); ?>"  placeholder="Digite novamente a senha"  required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Email</label>
                                                        <input type="email" class="form-control" id="email_user" name="email_user" placeholder="Digite o email" value="<?php echo $email; ?>" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Homepage</label>
                                                        <input type="text" class="form-control" id="homepage_user" name="homepage_user"  value="<?php echo utf8_encode($row['home_page']); ?>" placeholder="Digite o site, blog e etc...">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Telefone</label>
                                                        <input type="text" class="form-control telefone_user" id="telefone_user" name="telefone_user" value="<?php echo $telefone; ?>" placeholder="Digite o telefone">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Celular</label>
                                                        <input type="text" class="form-control celular_user" id="celular_user" name="celular_user" value="<?php echo $celular; ?>" placeholder="Digite o celular"  required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="cep" >CEP</label>
                                                        <input type="text" class="form-control cep_user" id="cep_user" name="cep_user" placeholder="Digite o cep"  value="<?php echo $cep; ?>" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <label for="estado"> Estado</label>
                                                        <select class="form-control m-bot15" id="estado_user" name="estado_user" required>
                                                            <option value="">Selecione o estado</option>
                                                            <option value="AC" <?php echo ($estado == 'AC')? ' selected ':' '; ?> >Acre</option>
                                                            <option value="AL" <?php echo ($estado == 'AL')? ' selected ':' '; ?> >Alagoas</option>
                                                            <option value="AP" <?php echo ($estado == 'AP')? ' selected ':' '; ?> >Amapá</option>
                                                            <option value="AM" <?php echo ($estado == 'AM')? ' selected ':' '; ?> >Amazonas</option>
                                                            <option value="BA" <?php echo ($estado == 'BA')? ' selected ':' '; ?> >Bahia</option>
                                                            <option value="CE" <?php echo ($estado == 'CE')? ' selected ':' '; ?> >Ceará</option>
                                                            <option value="DF" <?php echo ($estado == 'DF')? ' selected ':' '; ?> >Distrito Federal</option>
                                                            <option value="ES" <?php echo ($estado == 'ES')? ' selected ':' '; ?> >Espirito Santo</option>
                                                            <option value="GO" <?php echo ($estado == 'GO')? ' selected ':' '; ?> >Goiás</option>
                                                            <option value="MA" <?php echo ($estado == 'MA')? ' selected ':' '; ?> >Maranhão</option>
                                                            <option value="MS" <?php echo ($estado == 'MS')? ' selected ':' '; ?> >Mato Grosso do Sul</option>
                                                            <option value="MT" <?php echo ($estado == 'MT')? ' selected ':' '; ?> >Mato Grosso</option>
                                                            <option value="MG" <?php echo ($estado == 'MG')? ' selected ':' '; ?> >Minas Gerais</option>
                                                            <option value="PA" <?php echo ($estado == 'PA')? ' selected ':' '; ?> >Pará</option>
                                                            <option value="PB" <?php echo ($estado == 'PB')? ' selected ':' '; ?> >Paraíba</option>
                                                            <option value="PR" <?php echo ($estado == 'PR')? ' selected ':' '; ?> >Paraná</option>
                                                            <option value="PE" <?php echo ($estado == 'PE')? ' selected ':' '; ?> >Pernambuco</option>
                                                            <option value="PI" <?php echo ($estado == 'PI')? ' selected ':' '; ?> >Piauí</option>
                                                            <option value="RJ" <?php echo ($estado == 'RJ')? ' selected ':' '; ?> >Rio de Janeiro</option>
                                                            <option value="RN" <?php echo ($estado == 'RN')? ' selected ':' '; ?> >Rio Grande do Norte</option>
                                                            <option value="RS" <?php echo ($estado == 'RS')? ' selected ':' '; ?> >Rio Grande do Sul</option>
                                                            <option value="RO" <?php echo ($estado == 'RO')? ' selected ':' '; ?> >Rondônia</option>
                                                            <option value="RR" <?php echo ($estado == 'RR')? ' selected ':' '; ?> >Roraima</option>
                                                            <option value="SC" <?php echo ($estado == 'SC')? ' selected ':' '; ?> >Santa Catarina</option>
                                                            <option value="SP" <?php echo ($estado == 'SP')? ' selected ':' '; ?> >São Paulo</option>
                                                            <option value="SE" <?php echo ($estado == 'SE')? ' selected ':' '; ?> >Sergipe</option>
                                                            <option value="TO" <?php echo ($estado == 'TO')? ' selected ':' '; ?> >Tocantins</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="endereco">Endereço</label>
                                                <input type="text" class="form-control" id="endereco_user" name="endereco_user" placeholder="Digite o endereço"  value="<?php echo $endereco; ?>" required>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="bairro">Bairro</label>
                                                        <input type="text" class="form-control" id="bairro_user" name="bairro_user" placeholder="Digite o bairro"  value="<?php echo $bairro; ?>" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="cidade">Cidade</label>
                                                        <input type="text" class="form-control" id="cidade_user" name="cidade_user" placeholder="Digite a cidade"  value="<?php echo $cidade; ?>"  required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="estado">Usuário faz solicitação de exame ?</label>
                                                        <select class="form-control m-bot15" id="solicitacao_user" name="solicitacao_user" required>
                                                            <option value="">Selecione</option>
                                                            <option value="S" <?php echo ($row['tp_solicitacao'] =='S')? ' selected ':' '; ?> >Sim</option>
                                                            <option value="N" <?php echo ($row['tp_solicitacao'] =='N')? ' selected ':' '; ?> >Não</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="estado">Perfil</label>
                                                        <select class="form-control m-bot15" id="perfil_user" name="perfil_user" required>
                                                            <option value="">Selecione o perfil</option>
                                                            <option value="1"  <?php echo ($row['perfil'] =='1')? ' selected ':' '; ?> >Administrador</option>
                                                            <option value="2"  <?php echo ($row['perfil'] =='2')? ' selected ':' '; ?> >Colaborador</option>
                                                            <option value="3"  <?php echo ($row['perfil'] =='3')? ' selected ':' '; ?> >Usuário solicitante</option>                                                            
                                                            <option value="3"  <?php echo ($row['perfil'] =='5')? ' selected ':' '; ?> >Usuário executante</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6" align="right">
                                                    <div class="form-group">

                                                     <span class="btn btn-success fileinput-button">
                                                      <i class="glyphicon glyphicon-plus"></i>
                                                      <span>Adicionar foto</span>
                                                      <input id="file_imagem"  name="file_imagem" type="file"   />
                                                    </span>

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <img id="uploadPreview1" src="img/user/avatar-<?php echo(rand(1,5)); ?>.jpg" width="150" height="150" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="descricao">Observação</label>
                                                <textarea class="form-control" id="descricao" name="observacao_user" maxlength="500" rows="5"><?php echo utf8_encode($row['observacao']); ?></textarea>
                                            </div>

                                            <br>

                                            <div align="right">
                                                <button class="btn btn-danger btn_reset">Cancelar</button>
                                                <button type="submit" class="btn btn-success">Salvar</button>
                                            </div>



                                    </div>
                                </section>


                            </div>
                        </div>

                    </fieldset>
                    </form>





                </div>
            </div>
        </div>
        <!-- end:content -->

    </section>
</aside>
<!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->


<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h4>
            </div>
            <div class="modal-body" align="center">
                <h1 id="msg_modal" style="color: #2ECC71;">   </h1>
            </div>
            <div class="modal-footer"  align="right">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>
<!-- end modal  -->



</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Oct 2015 00:45:02 GMT -->
</html>