$(function(){

    $('.a_menu_exame_left').click();
	$('#valor').maskMoney();

    //converte o valor para double
    function valorMonetario (valor){
        if(valor!='' && valor!=undefined && valor!=null){
            var doubleValor = valor.split('.').join('');
            doubleValor = doubleValor.replace(',','.');
            return doubleValor;
        } else {
            return valor;
        }
    }



    //begin validate form =====================
    var form_validado = $("#form_exame").validate({
        submitHandler: function(form){

            var nome = $('#nome').val().trim();
            var valor = valorMonetario ($('#valor').val().trim());
            var rateio = $('#rateio').val().trim();
            var descricao = $('#descricao').val().trim();

            formDados = new FormData();
            formDados.append( 'controle', 'N' );
            formDados.append( 'nome', nome );
            formDados.append( 'valor', valor );
            formDados.append( 'rateio', rateio );
            formDados.append( 'descricao', descricao );

            //var dados = $(form).serialize();


            $.ajax({
                type: "POST",
                url: "model/grud_exame.php",
                data: formDados,
                 contentType: false,
                 processData: false,
                success: function( data )
                {
                    $('#msg_modal').html(data);
                    $('#modalForm').modal('show');

                    $("#form_exame")[0].reset();
                    $("#form_exame").find('.form-group').removeClass('has-error');
                    form_validado.resetForm();
                },
                error: function (){
                    $('#msg_modal').html('<i class="glyphicon glyphicon-warning-sign"></i> Erro ao se conectar ao servidor.');
                    $('#modalForm').modal('show');
                }
            });


        },
        rules: {
            valor: "required",
            rateio: "required",
            nome: {
                required: true,
                minlength: 3
            },
            descricao: "required"

        },
        messages: {
            valor: "Digite um valor para o exame.",
            rateio: "Digite um valor para o rateio médico.",
            nome: {
                required: "Digite o nome do exame.",
                minlength:"Digite o nome com mais de 2 caracteres."
            },
            descricao: "Digite a descrição do exame."

        }

    });






    $('.btn_reset').click(function(event) {
        event.preventDefault();
        $("#form_exame")[0].reset();
        $("#form_exame").find('.form-group').removeClass('has-error');
        form_validado.resetForm();
    });





});