$(function(){

    $('.a_menu_user_left').click();
    $('#uploadPreview1').attr('src', url_img_default);

    //mascara do form
    $('.telefone_user').mask('(00) 0000-0000');
    $('.celular_user').mask('(00) 00000-0000');
    $('.cep_user').mask('00000-000');

    $('#listaMedico').hide();

    //consulta medico
    $("#vinculoMedico").change(function() {
        var cd_medico = $(this).val();

        $.ajax({
            type: "POST",
            url: "model/consulta_medico_usuario.php",
            data: {cod_medico : cd_medico },
            success: function( data )
            {
                $('#c_medico').html(data);

                var nome_medico = $('#c_nome_medico').val();
                $('#nome_user').val(nome_medico).attr('readonly', 'readonly');

                var email_medico = $('#c_email_medico').val();
                $('#email_user').val(email_medico).attr('disabled','disabled');

                var telefone_medico = $('#c_telefone_medico').val();
                $('#telefone_user').val(telefone_medico).attr('disabled','disabled');

                var celular_medico = $('#c_celular_medico').val();
                $('#celular_user').val(celular_medico).attr('disabled','disabled');

                var endereco_medico = $('#c_endereco_medico').val();
                $('#endereco_user').val(endereco_medico).attr('disabled','disabled');

                var cidade_medico = $('#c_cidade_medico').val();
                $('#cidade_user').val(cidade_medico).attr('disabled','disabled');

                var bairro_medico = $('#c_bairro_medico').val();
                $('#bairro_user').val(bairro_medico).attr('disabled','disabled');

                var cep_medico = $('#c_cep_medico').val();
                $('#cep_user').val(cep_medico).attr('disabled','disabled');

                var estado_medico = $('#c_estado_medico').val();
                $('#estado_user').val(estado_medico).attr('disabled','disabled');

            },
            error: function (data){

            }
        });
    });



    //begin validate form ============================================
    var form_validado = $("#form_cad_usr").validate({
        submitHandler: function(form){

            var formData = new FormData(form);

            $.ajax({
                type: "POST",
                url: "model/grud_user.php",
                data: formData,
                processData: false,
                contentType: false,
                success: function( data )
                {
                    $('#msg_modal').html(data);
                    $('#modalForm').modal('show');

                    $("#form_cad_usr")[0].reset();
                    $("#form_cad_usr").find('.form-group').removeClass('has-error');
                    reset_form();
                    form_validado.resetForm();
                    $('#uploadPreview1').attr('src', url_img_default);
                },
                error: function (){
                    $('#msg_modal').html('<i class="glyphicon glyphicon-warning-sign"></i> Erro ao se conectar ao servidor.');
                    $('#modalForm').modal('show');
                }
            });


        },
        rules: {

            nome_user: {
                required: true,
                minlength: 3
            },
            login_user:{
                required: true,
                minlength: 3
            },
            senha_user: {
                required: true,
                minlength: 6
            },
            confirme_senha_user: {
                required: true,
                equalTo: "#senha_user",
                minlength: 6
            }

        },
        messages: {

            nome_user: {
                required: "Digite o nome.",
                minlength:"Digite o nome com mais de 4 caracteres."
            },
            login_user: {
                required: "Digite o login.",
                minlength:"Digite o login com mais de 4 caracteres."
            },
            senha_user:{
                required: "Digite uma senha.",
                minlength:"Digite uma senha com mais de 5 caracteres."
            },
            confirme_senha_user:{
                required: "Digite uma senha.",
                equalTo: "Digite uma senha igual a anterior.",
                minlength:"Digite uma senha com mais de 5 caracteres."
            },

            email_user: "Digite um email valido.",
            telefone_user: "Digite um telefone.",
            celular_user: "Digite um celular.",
            cep_user: "Digite um cep.",
            estado_user: "Selecione o estado.",
            bairro_user: "Digite o bairro.",
            endereco_user: "Digite o endereço.",
            cidade_user: " Digite a cidade.",
            banco_user: "Digite o nome do banco.",
            solicitacao_user: "Selecione uma opção.",
            perfil_user: "Selecione uma opção."

        }
    });



// end validate form ================

    $('.btn_reset').click(function(event) {
        event.preventDefault();
        reset_form();
        $("#form_cad_usr")[0].reset();
        $("#form_cad_usr").find('.form-group').removeClass('has-error');
        form_validado.resetForm();
    });




        if ($('input:radio[name="vinculo_user"]').is(":checked")){

            var v = $('input:radio[name="vinculo_user"]:checked').val();
            if (v==1){
                $('#listaMedico').show();
            }
            else {
                $('#vinculoMedico').val('');
                $('#listaMedico').hide();
                $('input:radio[name="vinculo_user"][value="2"]').attr("checked",true);
            }
        }


    $('input:radio[name="vinculo_user"]').click(function(){
        var v = $(this).val();
        if (v==1){
            $('#listaMedico').show();
            $("#form_cad_usr").find('.form-group').removeClass('has-error');
            form_validado.resetForm();
        }
        else {
            reset_form();
            $('input:radio[name="vinculo_user"][value="2"]').attr("checked",true);
        }

    });



    function reset_form(){
        $('#listaMedico').hide();
        $('#form_cad_usr')[0].reset();
        $.each( $('#form_cad_usr'),function(index, val) {
            $('input').removeAttr('disabled');
            $('input').removeAttr('readonly');
            $('select').removeAttr('disabled');
            $('#uploadPreview1').attr('src', url_img_default);
        });
    }




    $('#file_imagem').change(function(data){
        var arquivoBase = data.target.files[0];
        var tamanho_img = arquivoBase.size;
        var NovoFile = new FileReader();
        NovoFile.readAsDataURL(arquivoBase);
        NovoFile.onload = function (carregar) {
           var base64_img = carregar.target.result;
           $('#uploadPreview1').attr('src', base64_img);
        };




    });

    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage"+no).files[0]);

        var teste = document.getElementById("uploadImage"+no).files[0].size;
        //alert(teste);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview"+no).src = oFREvent.target.result;
        };
    }

});
