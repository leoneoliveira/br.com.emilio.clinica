$(function(){
	
	$('.a_menu_solic_left').click();

    //mascara do form
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.telefone').mask('(00) 0000-0000');
    $('.celular').mask('(00) 00000-0000');

	$('#dataInicial').datetimepicker({
        language:'pt-br', 
        pickTime: true                          
    });


	 //begin validate form =====================
    var form_validado = $("#form_solic").validate({
        submitHandler: function(form){

            var dados = $(form).serialize();
            $.ajax({
                type: "POST",
                url: "model/grud_solicitacao.php",
                data: dados,
                success: function( data )
                {;
                    $('#msg_modal').html(data);
                    $('#modalForm').modal('show');
                    
                    $('.lista-exames').html('');
                    $("#form_solic")[0].reset();
                    $("#form_solic").find('.form-group').removeClass('has-error');
                    form_validado.resetForm();
                },
                error: function (){
                    $('#msg_modal').html('<i class="glyphicon glyphicon-warning-sign"></i> Erro ao se conectar ao servidor.');
                    $('#modalForm').modal('show');
                }
            });


        },
        rules: {
            nome_medico: "required",
            data_hora: "required",
            nome_paciente: "required",
            cpf_paciente: {
                required: true,
                validarCPF: true
            },
            telefone: "required",
            celular: "required",
            descricao: "required"
        },
        messages: {
            nome_medico: "Digite o nome do medico solicitante",
            data_hora: "Digite a data e hora para realização do exame",
            nome_paciente: "Digite o nome do paciente",
            telefone: "Digite o telefone do paciente",
            celular: "Digite o celular do paciente",
            descricao: "Digite a Observação.",
            cpf_paciente: {
                required: "Digite o CPF do paciente"
            }
        }

    });

    $.validator.addMethod('validarCPF', function(value, element) {
        var cpf = value;
        var retorno = validarCPF(cpf);
        return retorno;
    }, "CPF Inválido.");

    function validarCPF(cpf) {
        var cpf = cpf.replace(/[^\d]+/g,'');
        if(cpf == '') return false;
        // Elimina CPFs invalidos conhecidos
        if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
            return false;
        // Valida 1o digito
        var add = 0;
        var rev;
        for (i=0; i < 9; i ++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
        // Valida 2o digito
        add = 0;
        for (i = 0; i < 10; i ++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;
        return true;
    }






	$('.btn_reset').click(function(event) {
        event.preventDefault();
        $("#form_solic")[0].reset();
        $("#form_solic").find('.form-group').removeClass('has-error');
        form_validado.resetForm();
    });



   $('.btn_add_exame').click(function(event) {
        event.preventDefault();
        var ex = $('#exame_select');
        var val = $('#exame_select').val();

        if (val!=='' && val!==undefined) {
            var valor = ex.val();
            var texto = ex.find('option:selected').text().trim();

            $('.lista-exames').append(
                $('<li class="list-group-item" >').text(texto).attr('data-val', valor) .append(
                    '<button type="button" class="btn btn-danger btn-xs excluirExame" style="float: right;margin-left: 10px;">'
                    +'<i class="fa fa-trash-o"></i>'
                    +'</button> '
                )
            );  

            ex.val('');


            var val_exames = '';
            $('.lista-exames').find('li[data-val]').each(function(){
                if(val_exames !=''){
                    val_exames = val_exames +'|'+ $(this).attr('data-val');
                } else {
                    val_exames = $(this).attr('data-val');
                }
            });
            $('#exames').val(val_exames);


        } else {

        }

   });



    $('body').on( "click", ".excluirExame",function(event) {
        event.preventDefault();
        var elem = $(this).parent('li');
        $(elem).remove();

        var val_exames = '';
        $('.lista-exames').find('li[data-val]').each(function(){
            if(val_exames !=''){
                val_exames = val_exames +'|'+ $(this).attr('data-val');
            } else {
                val_exames = $(this).attr('data-val');
            }
        });
        $('#exames').val(val_exames);

    });


});