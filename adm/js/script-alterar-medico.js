$(function(){
  //abrir menu medico
  $('.a_menu_medico_left').click();


  //mascara do form
  $('.cpf').mask('000.000.000-00', {reverse: true});
  $('.telefone').mask('(00) 0000-0000');
  $('.celular').mask('(00) 00000-0000');
  $('.cep').mask('00000-000');

  //begin validate form =====================
  var form_validado = $("#form_medico").validate({
            submitHandler: function(form){

                var dados = $(form).serialize();

                $.ajax({
                  type: "POST",
                  url: "model/grud_medico.php",
                  data: dados,
                  success: function( data )
                  {
                      $('#msg_modal').html(data);
                      $('#modalForm').modal('show');

                      // $("#form_medico")[0].reset();
                      // $("#form_medico").find('.form-group').removeClass('has-error');
                      // form_validado.resetForm();
                  },
                  error: function (){
                    $('#msg_modal').html('<i class="glyphicon glyphicon-warning-sign"></i> Erro ao se conectar ao servidor.');
                    $('#modalForm').modal('show');
                  }
                });


            },
            rules: {
                crm: "required",
                nome: {
                    required: true,
                    minlength: 4
                },
                cpf: {
                    required: true,
                    validarCPF: true
                }

            },
            messages: {
                especialidade: "Digite a especialidade médica.",
                crm: "Digite um CRM Ex: CRM 0100",
                nome: {
                    required: "Digite o nome do médico.",
                    minlength:"Digite o nome com mais de 4 caracteres."
                },
                cpf: {
                    required: "Digite o CPF"
                },
                email: "Digite um email valido.",
                telefone: "Digite um telefone.",
                celular: "Digite um celular.",
                cep: "Digite um cep.",
                estado: "Selecione o estado.",
                bairro: "Digite o bairro.",
                endereco: "Digite o endereço.",
                cidade: " Digite a cidade.",
                banco: "Digite o nome do banco.",
                agencia: "Digite o número da agência.",
                conta: "Digite o número da conta."
            }
        });

        $.validator.addMethod('validarCPF', function(value, element) {
            var cpf = value;
            var retorno = validarCPF(cpf);
            return retorno;
        }, "CPF Inválido.");

// end validate form ================

$('.btn_reset').click(function(event) {
    event.preventDefault();
    window.location.assign("lista_medicos.php");

    // $("#form_medico")[0].reset();
    // $("#form_medico").find('.form-group').removeClass('has-error');
    // form_validado.resetForm();
});

//ao fechar o modal redireciona para lista medico
$('#modalForm').on('hidden.bs.modal', function (e) {
  window.location.assign("lista_medicos.php");
});



function validarCPF(cpf) {
  var cpf = cpf.replace(/[^\d]+/g,'');
  if(cpf == '') return false;
  // Elimina CPFs invalidos conhecidos
  if (cpf.length != 11 ||
    cpf == "00000000000" ||
    cpf == "11111111111" ||
    cpf == "22222222222" ||
    cpf == "33333333333" ||
    cpf == "44444444444" ||
    cpf == "55555555555" ||
    cpf == "66666666666" ||
    cpf == "77777777777" ||
    cpf == "88888888888" ||
    cpf == "99999999999")
    return false;
    // Valida 1o digito
    var add = 0;
    var rev;
    for (i=0; i < 9; i ++)
    add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
    rev = 0;
    if (rev != parseInt(cpf.charAt(9)))
    return false;
    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i ++)
    add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
    rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
    return false;
    return true;
  }


});
