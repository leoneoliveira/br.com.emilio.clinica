<?php
include '../verifica.php';
include('../conexao/config.php');

if (isset($_POST['cod_exame'])) {
    $id =  utf8_decode($_POST['cod_exame']);
}else{
    $id = '';
}

if (isset($_POST['nome'])) {
    $nome =  utf8_decode($_POST['nome']);
}else{
    $nome = '';
}

if (isset($_POST['valor'])) {
    $valor =  utf8_decode($_POST['valor']);
}else{
    $valor = '';
}

if (isset($_POST['descricao'])) {
    $descricao =  utf8_decode($_POST['descricao']);
}else{
    $descricao = '';
}


if (isset($_POST['status'])) {
    $status =  utf8_decode($_POST['status']);
}else{
    $status = 'A';
}


if (isset($_POST['controle'])) {
    $controle =  utf8_decode($_POST['controle']);
}else{
    $controle = '';
}


if (isset($_POST['user_cad'])) {
    $user_cad =  utf8_decode($_POST['user_cad']);
}else{
    $user_cad = $_SESSION['usr_cd_user'];
}

if (isset($_POST['user_alt'])) {
    $user_alt =  utf8_decode($_POST['user_alt']);
}else{
    $user_alt = $_SESSION['usr_cd_user'];
}

if (isset($_POST['rateio'])) {
    $rateio =  utf8_decode($_POST['rateio']);
}else{
    $rateio = '';
}

//sql para cadastrar exame
$sql_insert = "INSERT INTO exame (
  nome,
  valor,
  rateio,
  descricao,
  user_cad,
  dt_cad,
  status )
VALUES
( '$nome',
  '$valor',
  '$rateio',
  '$descricao',
  '$user_cad',
    NOW(),
  '$status')";


//sql para alterar exame
$sql_update = "UPDATE exame SET
nome = '$nome',
valor = '$valor',
rateio = '$rateio',
descricao = '$descricao',
user_alt = '$user_alt',
dt_alt = NOW(),
status = '$status'
WHERE
cd_exame = '$id'";


//sql para alterar status para inativo
$sql_inativo = "UPDATE exame SET
status='I',
user_alt = '$user_alt',
dt_alt = NOW()
WHERE
cd_exame= '$id'";

//sql para alterar status para excluido
$sql_excluir = "UPDATE exame SET
status='E',
user_alt = '$user_alt',
dt_alt = NOW()
WHERE
cd_exame= '$id'";

//sql para alterar status para ativo
$sql_ativar = "UPDATE exame SET
status='A',
user_alt = '$user_alt',
dt_alt = NOW()
WHERE
cd_exame= '$id'";


if($controle == 'N'){
    //novo exame
    if (mysqli_query($conn, $sql_insert)) {
        echo ' <i class="glyphicon glyphicon-ok"></i> Cadastrado com sucesso!';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

} elseif ($controle == 'M' AND $id!=='' AND $id!==null) {
    // alterar exame
    if (mysqli_query($conn, $sql_update)) {
        echo "Alterado com sucesso!";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

} elseif ($controle == 'I' AND $id!=='' AND $id!==null) {
    //inativa exame
    if (mysqli_query($conn, $sql_inativo)) {
        echo "Inativado com sucesso!";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

} elseif ($controle == 'E' AND $id!=='' AND $id!==null) {
    //excluir exame
    if (mysqli_query($conn, $sql_excluir)) {
        echo "Excluido com sucesso!";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}elseif ($controle == 'A' AND $id!=='' AND $id!==null) {
    //ativar exame
    if (mysqli_query($conn, $sql_ativar)) {
        echo "Ativado com sucesso!";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}




mysqli_close($conn);
