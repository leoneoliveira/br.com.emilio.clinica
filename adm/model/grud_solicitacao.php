<?php
include '../verifica.php';
include('../conexao/config.php');



if (isset($_POST['cd_solicitacao'])) {
     $cd_solicitacao =  utf8_decode($_POST['cd_solicitacao']);
}else{
     $cd_solicitacao = '';
}


if (isset($_POST['nome_medico'])) {
     $nome_medico =  utf8_decode($_POST['nome_medico']);
}else{
     $nome_medico = '';
}

if (isset($_POST['data_hora'])) {
    $data_hora =  utf8_decode($_POST['data_hora']);
}else{
    $data_hora = '';
}

if (isset($_POST['nome_paciente'])) {
    $nome_paciente =  utf8_decode($_POST['nome_paciente']);
}else{
    $nome_paciente = '';
}

if (isset($_POST['cpf_paciente'])) {
    $cpf_paciente =  utf8_decode($_POST['cpf_paciente']);
}else{
    $cpf_paciente = '';
}

if (isset($_POST['telefone'])) {
    $telefone =  utf8_decode($_POST['telefone']);
}else{
    $telefone = '';
}

if (isset($_POST['celular'])) {
    $celular =  utf8_decode($_POST['celular']);
}else{
    $celular = '';
}

if (isset($_POST['exames'])) {
    $exames =  utf8_decode($_POST['exames']);
}else{
    $exames = '';
}

if (isset($_POST['descricao'])) {
    $descricao =  utf8_decode($_POST['descricao']);
}else{
    $descricao = '';
}

if (isset($_POST['cd_medico_executante'])) {
    $cd_medico_executante =  utf8_decode($_POST['cd_medico_executante']);
}else{
    $cd_medico_executante = '';
}


if (isset($_POST['controle'])) {
    $controle =  utf8_decode($_POST['controle']);
}else{
    $controle = '';
}


$usr_cad = $_SESSION['usr_cd_user'];

$sql_insert = "INSERT INTO solicitacao(
		dt_solicitacao,
		nm_paciente,
		cd_medico_solicitante, 
		cpf_paciente,
		telefone, 
		celular, 
		exames, 
		descricao, 
		status, 
		user_cad,		 
		dt_cad
		)
VALUES (
        STR_TO_DATE('$data_hora', '%d/%m/%Y %H:%i'),
        '$nome_paciente',
        '$nome_medico',
        '$cpf_paciente',
        '$telefone',
        '$celular',
        '$exames',
        '$descricao',
        'AC',
        '$usr_cad',
         NOW()
        )";

//sql para alterar status para inativo
$sql_status = "UPDATE solicitacao SET
status='$controle',
user_alt = '$usr_cad',
dt_alt = NOW()
WHERE
cd_solicitacao= '$cd_solicitacao'";

//sql para alterar medico executante
$sql_med_execut = "UPDATE solicitacao SET
user_alt = '$usr_cad',
dt_alt = NOW(),
cd_medico_executante = $cd_medico_executante
WHERE
cd_solicitacao= '$cd_solicitacao'";

if($controle == 'N'){
    //novo cadastro de usuario
    if (mysqli_query($conn, $sql_insert)) {
        echo ' <i class="glyphicon glyphicon-ok"></i> Cadastrado com sucesso!';
    } else {
        echo "Error: " . $sql_insert . "<br>" . mysqli_error($conn);
    }
}

if($controle == 'A'){
    //Confirmar 
    if (mysqli_query($conn, $sql_status)) {
        echo ' <i class="glyphicon glyphicon-ok"></i> Agendamento confirmado com sucesso!';
    } else {
        echo "Error: " . $sql_status . "<br>" . mysqli_error($conn);
    }
}

if($controle == 'I'){
    //Confirmar 
    if (mysqli_query($conn, $sql_status)) {
        echo ' <i class="glyphicon glyphicon-ok"></i> Agendamento cancelado com sucesso!';
    } else {
        echo "Error: " . $sql_status . "<br>" . mysqli_error($conn);
    }
}

if($controle == 'E'){
    //Confirmar 
    if (mysqli_query($conn, $sql_status)) {
        echo ' <i class="glyphicon glyphicon-ok"></i> Agendamento excluido com sucesso!';
    } else {
        echo "Error: " . $sql_status . "<br>" . mysqli_error($conn);
    }
}

if($controle == 'AC'){
    //Confirmar 
    if (mysqli_query($conn, $sql_status)) {
        echo ' <i class="glyphicon glyphicon-ok"></i> Agendamento Pendente!';
    } else {
        echo "Error: " . $sql_status . "<br>" . mysqli_error($conn);
    }
}

if($controle == 'med'){
    if (mysqli_query($conn, $sql_med_execut)) {
        echo 'OK';
    } else {
        echo "Error: " . $sql_status . "<br>" . mysqli_error($conn);
    }
}

