<?php
include '../verifica.php';
include('../conexao/config.php');


if (isset($_POST['id'])) {
    $id =  utf8_decode($_POST['id']);
}else{
    $id = '';
}

if (isset($_POST['vinculo_user'])) {
    $vinculo_user =  utf8_decode($_POST['vinculo_user']);
}else{
    $vinculo_user = '';
}

if (isset($_POST['id_medico'])) {
    $cod_medico =  utf8_decode($_POST['id_medico']);
}else{
    $cod_medico = '';
}

if (isset($_POST['nome_user'])) {
    $nome_user =  utf8_decode($_POST['nome_user']);
}else{
    $nome_user = '';
}

if (isset($_POST['login_user'])) {
    $login_user =  utf8_decode($_POST['login_user']);
}else{
    $login_user = '';
}

if (isset($_POST['senha_user'])) {
    $senha_user =  utf8_decode($_POST['senha_user']);
}else{
    $senha_user = '';
}

if (isset($_POST['confirme_senha_user'])) {
    $confirme_senha_user =  utf8_decode($_POST['confirme_senha_user']);
}else{
    $confirme_senha_user = '';
}

if (isset($_POST['email_user'])) {
    $email_user =  utf8_decode($_POST['email_user']);
}else{
    $email_user = '';
}

if (isset($_POST['homepage_user'])) {
    $homepage_user =  utf8_decode($_POST['homepage_user']);
}else{
    $homepage_user = '';
}

if (isset($_POST['telefone_user'])) {
    $telefone_user =  utf8_decode($_POST['telefone_user']);
}else{
    $telefone_user = '';
}

if (isset($_POST['celular_user'])) {
    $celular_user =  utf8_decode($_POST['celular_user']);
}else{
    $celular_user = '';
}

if (isset($_POST['cep_user'])) {
    $cep_user =  utf8_decode($_POST['cep_user']);
}else{
    $cep_user = '';
}

if (isset($_POST['estado_user'])) {
    $estado_user =  utf8_decode($_POST['estado_user']);
}else{
    $estado_user = '';
}

if (isset($_POST['endereco_user'])) {
    $endereco_user =  utf8_decode($_POST['endereco_user']);
}else{
    $endereco_user = '';
}

if (isset($_POST['bairro_user'])) {
    $bairro_user =  utf8_decode($_POST['bairro_user']);
}else{
    $bairro_user = '';
}

if (isset($_POST['cidade_user'])) {
    $cidade_user =  utf8_decode($_POST['cidade_user']);
}else{
    $cidade_user = '';
}

if (isset($_POST['solicitacao_user'])) {
    $tp_solicitacao =  utf8_decode($_POST['solicitacao_user']);
}else{
    $tp_solicitacao = '';
}


if (isset($_POST['perfil_user'])) {
    $perfil_user =  utf8_decode($_POST['perfil_user']);
}else{
    $perfil_user = '';
}

if (isset($_POST['observacao_user'])) {
    $observacao_user =  utf8_decode($_POST['observacao_user']);
}else{
    $observacao_user = '';
}


if (isset($_POST['status'])) {
    $status =  utf8_decode($_POST['status']);
}else{
    $status = 'A';
}

if (isset($_POST['controle'])) {
    $controle =  utf8_decode($_POST['controle']);
}else{
    $controle = '';
}


if(isset($_FILES['file_imagem'])){
    if (!empty($_FILES['file_imagem']['name'])) {
        include 'upload_file.php';
    }else{ $img_perfil = ''; }
} else {
    $img_perfil = '';
}



$usr_cad = $_SESSION['usr_cd_user'];

$user_alt = $_SESSION['usr_cd_user'];



$sql_insert = "INSERT INTO user (
 nome,
 login,
 senha,
 email,
 home_page,
 telefone,
 celular,
 cep,
 endereco,
 bairro,
 cidade,
 estado,
 observacao,
 tp_solicitacao,
 perfil,
 cd_medico,
 usr_cad,
 dt_cad,
 status,
 img_perfil,
vinculo_medico)
VALUES
('$nome_user',
'$login_user',
'$senha_user',
'$email_user',
'$homepage_user',
'$telefone_user',
'$celular_user',
'$cep_user',
'$endereco_user',
'$bairro_user',
'$cidade_user',
'$estado_user',
'$observacao_user',
'$tp_solicitacao',
'$perfil_user',
'$cod_medico',
'$usr_cad',
NOW(),
'$status',
'$img_perfil',
'$vinculo_user')";




$sql_update = '';


//sql update usuario com aletracao de foto

$sql_updateA = "UPDATE user SET
 nome = '$nome_user',
 login = '$login_user',
 senha = '$senha_user',
 email = '$email_user',
 home_page = '$homepage_user',
 telefone = '$telefone_user',
 celular = '$celular_user',
 cep = '$cep_user',
 endereco = '$endereco_user',
 bairro = '$bairro_user',
 cidade = '$cidade_user',
 estado = '$estado_user',
 observacao = '$observacao_user',
 tp_solicitacao = '$tp_solicitacao',
 perfil = '$perfil_user',
 cd_medico = '$cod_medico',
 usr_alt = '$user_alt',
 dt_alt = NOW(),
img_perfil = '$img_perfil',
vinculo_medico = '$vinculo_user'
WHERE
cd_user = '$id'";


//sql update usuario sem aletracao de foto

$sql_updateB = "UPDATE user SET
 nome = '$nome_user',
 login = '$login_user',
 senha = '$senha_user',
 email = '$email_user',
 home_page = '$homepage_user',
 telefone = '$telefone_user',
 celular = '$celular_user',
 cep = '$cep_user',
 endereco = '$endereco_user',
 bairro = '$bairro_user',
 cidade = '$cidade_user',
 estado = '$estado_user',
 observacao = '$observacao_user',
 tp_solicitacao = '$tp_solicitacao',
 perfil = '$perfil_user',
 cd_medico = '$cod_medico',
 usr_alt = '$user_alt',
 dt_alt = NOW(),
 vinculo_medico = '$vinculo_user'
WHERE
cd_user = '$id'";



if($img_perfil != '' AND $img_perfil != null ){
    $sql_update = $sql_updateA;
} else {
    $sql_update = $sql_updateB;
}

//sql inativo
$sql_inativo = "UPDATE user SET
status = 'I',
usr_alt = '$user_alt',
dt_alt = NOW()
WHERE
cd_user = '$id'";

//sql excluir
$sql_excluir = "UPDATE user SET
status = 'E',
usr_alt = '$user_alt',
dt_alt = NOW()
WHERE
cd_user = '$id'";

//sql ativar
$sql_ativar = "UPDATE user SET
status = 'A',
usr_alt = '$user_alt',
dt_alt = NOW()
WHERE
cd_user = '$id'";


if($controle == 'N'){
    //novo cadastro de usuario
    if (mysqli_query($conn, $sql_insert)) {
        echo ' <i class="glyphicon glyphicon-ok"></i> Cadastrado com sucesso!';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

} elseif ($controle == 'M' AND $id!=='' AND $id!==null) {
    // alterar cadastro
    if (mysqli_query($conn, $sql_update)) {
        echo "Alterado com sucesso!";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

} elseif ($controle == 'I' AND $id!=='' AND $id!==null) {
    //inativa cadastro
    if (mysqli_query($conn, $sql_inativo)) {
        echo "Inativado com sucesso!";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

} elseif ($controle == 'E' AND $id!=='' AND $id!==null) {
    //excluir cadastro
    if (mysqli_query($conn, $sql_excluir)) {
        echo "Excluido com sucesso!";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}elseif ($controle == 'A' AND $id!=='' AND $id!==null) {
    //ativar
    if (mysqli_query($conn, $sql_ativar)) {
        echo "Ativado com sucesso!";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

}





mysqli_close($conn);

?>