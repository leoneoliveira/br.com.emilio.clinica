-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20-Jan-2016 às 00:25
-- Versão do servidor: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinica`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `exame`
--

CREATE TABLE `exame` (
  `cd_exame` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` varchar(1000) DEFAULT NULL,
  `valor` double(15,2) DEFAULT NULL,
  `rateio` int(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `user_cad` varchar(100) NOT NULL,
  `user_alt` varchar(100) DEFAULT NULL,
  `dt_cad` date NOT NULL,
  `dt_alt` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `exame`
--

INSERT INTO `exame` (`cd_exame`, `nome`, `descricao`, `valor`, `rateio`, `status`, `user_cad`, `user_alt`, `dt_cad`, `dt_alt`) VALUES
(1, 'Exame 1 Alteração', 'Alteração\r\nNovo exame sendo cadastrado para teste', 25.05, 60, 'A', 'usuario teste', '3', '2015-11-08', '2016-01-17'),
(2, 'Outro exame', 'Novo teste', 1500.00, 45, 'A', 'usuario teste', '3', '2015-11-08', '2016-01-17'),
(3, 'Exame 2', 'lorem', 3800.00, 35, 'A', 'usuario teste', '3', '2015-11-08', '2016-01-17'),
(4, 'Não sei o nome', 'lorem', 464.22, 10, 'A', 'usuario teste', '3', '2015-11-08', '2016-01-17'),
(5, 'Cardio', 'Coração', 222.22, 30, 'A', '3', '3', '2016-01-06', '2016-01-17'),
(6, 'Eletro', 'TESTE', 500.00, 50, 'A', '3', '3', '2016-01-06', '2016-01-17'),
(7, 'Test', 'sasasasas', 800.00, 5, 'A', '3', '3', '2016-01-06', '2016-01-17'),
(8, 'rx de eito', 'Jejum', 300.00, 2, 'A', '3', '3', '2016-01-17', '2016-01-17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `medico`
--

CREATE TABLE `medico` (
  `cd_medico` int(10) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `crm` varchar(50) NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `telefone` varchar(30) NOT NULL,
  `celular` varchar(30) NOT NULL,
  `email` varchar(200) NOT NULL,
  `cep` varchar(50) NOT NULL,
  `endereco` varchar(150) NOT NULL,
  `bairro` varchar(150) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `banco` varchar(150) NOT NULL,
  `agencia` varchar(50) NOT NULL,
  `conta` varchar(50) NOT NULL,
  `user_cad` varchar(20) NOT NULL,
  `user_alt` varchar(20) NOT NULL,
  `dt_cad` date NOT NULL,
  `dt_alt` date NOT NULL,
  `status` varchar(10) NOT NULL,
  `especialidade` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `medico`
--

INSERT INTO `medico` (`cd_medico`, `nome`, `crm`, `cpf`, `telefone`, `celular`, `email`, `cep`, `endereco`, `bairro`, `cidade`, `estado`, `banco`, `agencia`, `conta`, `user_cad`, `user_alt`, `dt_cad`, `dt_alt`, `status`, `especialidade`) VALUES
(8, 'Novo Nome', 'CRM5646', '277.178.668-03', '(84) 5555-5555', '(55) 55555-5555', 'teste@teste.com', '44444-444', 'teste', 'teste', 'teste', 'PB', '   teste', '564564654', '654564', 'usuario teste', 'usuario alter', '2015-10-24', '2015-11-02', 'A', NULL),
(9, 'Fulano médico ', 'CRM 235767', '141.568.461-80', '(84) 6556-4545', '(97) 89988-9999', 'teste@teste.com', '84546-546', 'Nova rua ', 'centro', 'recife', 'PE', 'Brasil ', '45687', '546546546546', 'usuario teste', '3', '2015-11-02', '2016-01-05', 'E', NULL),
(10, 'Outro Médico', 'CRM 56765', '656.982.357-40', '(84) 5654-6546', '(65) 46546-4654', 'teste@teste.com', '88787-878', 'rua tempo novo', 'centro', 'Recife', 'PE', 'Caixa', '5645465', '64564654654', 'usuario teste', 'usuario alter', '2015-11-02', '2015-11-02', 'E', NULL),
(11, 'gfhfhfgh', '444444', '536.633.662-78', '(44) 4444-4444', '(66) 66666-6666', 'teste@neto.com', '55555-555', 'hjkhhks', 'dfsdfsfsdf', 'fghfhgss', 'ES', 'sdfdsf', 'sdfsdf', 'sdfsdfds', 'usuario teste', '', '2015-11-17', '0000-00-00', 'A', 'hhjghjghj');

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacao`
--

CREATE TABLE `solicitacao` (
  `cd_solicitacao` int(10) NOT NULL,
  `mn_medico` varchar(100) NOT NULL,
  `nm_paciente` varchar(100) NOT NULL,
  `dt_solicitacao` varchar(50) NOT NULL,
  `cpf_paciente` varchar(100) NOT NULL,
  `telefone` varchar(100) NOT NULL,
  `celular` varchar(100) NOT NULL,
  `exames` varchar(100) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `user_cad` varchar(100) NOT NULL,
  `user_alt` varchar(100) NOT NULL,
  `dt_cad` date NOT NULL,
  `dt_alt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `solicitacao`
--

INSERT INTO `solicitacao` (`cd_solicitacao`, `mn_medico`, `nm_paciente`, `dt_solicitacao`, `cpf_paciente`, `telefone`, `celular`, `exames`, `descricao`, `status`, `user_cad`, `user_alt`, `dt_cad`, `dt_alt`) VALUES
(5, 'leone pinheiro de oliveira', 'leone pinheiro de oliveira', '03/01/2016 21:44', '088.301.844-62', '0', '0', '1', '0', 'I', '3', '3', '2016-01-03', '2016-01-10'),
(6, 'leo', 'leone pinheiro de oliveira', '03/01/2016 21:46', '088.301.844-62', '(66) 6666-6666', '(66) 66666-6666', '1|1|4', 'sasasas', 'AC', '3', '', '2016-01-03', '0000-00-00'),
(7, 'Emilio', 'Leone Oliveira', '05/01/2016 13:39', '088.301.844-62', '(81) 3445-5984', '(81) 99659-0490', '1|4', 'ECO + DOPLE', 'E', '3', '3', '2016-01-05', '2016-01-10'),
(8, 'Emilio', 'leone pinheiro de oliveira', '21/01/2016 14:00', '088.301.844-62', '(22) 2222-2222', '(22) 22222-2222', '1|4|1|4', 'AC + FE +dad', 'A', '3', '3', '2016-01-05', '2016-01-10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `cd_user` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `login` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `home_page` varchar(200) DEFAULT NULL,
  `telefone` varchar(40) DEFAULT NULL,
  `celular` varchar(40) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `bairro` varchar(20) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  `observacao` varchar(550) DEFAULT NULL,
  `tp_solicitacao` varchar(100) DEFAULT NULL,
  `perfil` varchar(100) DEFAULT NULL,
  `cd_medico` varchar(100) DEFAULT NULL,
  `usr_cad` varchar(20) NOT NULL,
  `usr_alt` varchar(20) NOT NULL,
  `dt_cad` date NOT NULL,
  `dt_alt` date NOT NULL,
  `status` varchar(5) NOT NULL,
  `img_perfil` varchar(255) DEFAULT NULL,
  `cep` varchar(200) DEFAULT NULL,
  `vinculo_medico` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`cd_user`, `nome`, `login`, `senha`, `email`, `home_page`, `telefone`, `celular`, `endereco`, `bairro`, `cidade`, `estado`, `observacao`, `tp_solicitacao`, `perfil`, `cd_medico`, `usr_cad`, `usr_alt`, `dt_cad`, `dt_alt`, `status`, `img_perfil`, `cep`, `vinculo_medico`) VALUES
(3, 'Novo Nome', 'leone2', '123456', '', 'www.teste.com', '', '', '', '', '', '', 'ssssssss', 'S', '2', '8', 'User teste', 'User alteracao', '2015-11-17', '2015-11-18', 'A', '8ee386fbd115c683f87b897d559459b5.png', '', '1'),
(4, 'Novo Nome', 'leone2', '123456', '', 'www.teste.com', '', '', '', '', '', '', 'gggggg', 'S', '2', '8', 'User teste', 'User alteracao', '2015-11-17', '2015-11-18', 'A', '6ef5c38490290db28bce88a453026fa4.jpg', '', '1'),
(6, 'gfhfhfgh', 'login', '123456', '', '', '', '', '', '', '', '', 'fgfgfgfdgdgf', 'S', '3', '11', 'User teste', '', '2015-11-18', '0000-00-00', 'A', 'a344e8e431bb0d5b155cd8ae2a5fd4fa.png', '', NULL),
(7, 'Saimo', 'Saimo', '123456', 'teste@teste.com', '', '(55) 6566-6666', '(99) 99999-9999', 'rua Saimo', 'bairro Saimo', 'cidade Saimo', 'AM', 'Observação Saimo', 'S', '1', '', 'User teste', '', '2015-11-18', '0000-00-00', 'A', '723e94af0c04fe339fdfcb1d50eab75b.jpg', '87877-777', '2'),
(8, 'adm@com', 'adm@com', '123456', 'theleoneoliveira@gma', '', '(00) 0000-0000', '(00) 00000-0000', 'rua campos sales 196', 'madalen', 'recife', 'PE', '', 'S', '1', '', '3', '', '2016-01-06', '0000-00-00', 'A', '24e06f9b140c5ed6a0962366d86c8d84.jpg', '50610-400', '2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exame`
--
ALTER TABLE `exame`
  ADD PRIMARY KEY (`cd_exame`);

--
-- Indexes for table `medico`
--
ALTER TABLE `medico`
  ADD PRIMARY KEY (`cd_medico`);

--
-- Indexes for table `solicitacao`
--
ALTER TABLE `solicitacao`
  ADD PRIMARY KEY (`cd_solicitacao`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`cd_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exame`
--
ALTER TABLE `exame`
  MODIFY `cd_exame` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `medico`
--
ALTER TABLE `medico`
  MODIFY `cd_medico` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `solicitacao`
--
ALTER TABLE `solicitacao`
  MODIFY `cd_solicitacao` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `cd_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
