<?php include 'head.php'; ?>
<?php include 'conexao/config.php' ?>

<?php 

$query = "SELECT * FROM medico order by cd_medico desc";
$result = mysqli_query($conn, $query);
$total_num_rows = mysqli_num_rows($result);


function situacao($args){
    switch ($args) {
        case 'A':
        $situacao =  '<span class="label label-success">Ativo';
        break;
        case 'I':
        $situacao = '<span class="label label-info">Inativo</span>';
        break;
        case 'E':
        $situacao = '<span class="label label-danger">Excluído</span>';
        break;
    }

    echo $situacao;
}

function linhaStatus ($args){
    switch ($args) {
        case 'A':
        $class =  '';
        break;
        case 'I':
        $class = ' class="warning" ';
        break;
        case 'E':
        $class = ' class="danger" ';
        break;
        default:
        $class =  '';
    }

    echo $class;
}

?>
<body class="cl-default fixed">

    <link href="plugins/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />

    <link rel="stylesheet" href="plugins/data-tables/DT_bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="css/table-responsive.css">

    <style>

    .warning {
        background-color: #F9EBA2 !important;
    }

    tr.warning > td {
        background-color: #F9EBA2 !important;
    }

    .danger{
        background-color: #F69E9E !important;
    }

    tr.danger > td {
        background-color: #F69E9E !important;
    }

    </style>


    <!-- inicio:navbar top -->
    <?php include 'head_menu_top.php'; ?>
    <!-- Fin:navbar top -->
    
    <!-- inicio: Toda Lateral do menu -parametro($vMenu) -->
    <?php include 'head_menu_left.php'; ?>
    <!--  Toda Lateral do menu -->


    <script src="plugins/data-tables/dataModificado/jquery.dataTables.js"></script>
    <script src="plugins/data-tables/dataModificado/dataTables.bootstrap.js"></script>


    <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {

        $('.a_menu_medico_left').click();


        $('#lista_medicos').dataTable( {
            "pageLength": 10,
            "lengthChange": false,  
                        "order": [[ 3, "desc" ]], //ordena por coluna 
                        "language": {
                            "url": "plugins/data-tables/dataModificado/Portuguese-Brasil.json" //tradução para português
                        },
                        stateSave: true, //salvar pesquisa em tempo 
                        "searching": true //oculta ou mostra
                    }); 


        //alterar_medico.php


        //alterar medico
        $('.aletar_med').click(function(){
            var cod = $(this).attr('data-cod');
            var form = $('#form_enviar'); 

            if (cod!='' && cod!=null && cod!=undefined ) {

                $('#controle').val('M');
                $('#cod_medico').val(cod); 
                form.attr('action', 'alterar_medico.php');                
                form.submit();
            }

        });

        //inativar medico
        $('.inativar_med').click(function(){
            var cod = $(this).attr('data-cod');

            console.log('inativar_med');

            if (cod!='' && cod!=null && cod!=undefined ) {

                $.ajax({
                  method: "POST",
                  url: "model/consulta_nome_medico.php",
                  data: { cod_medico: cod },
                  success: function( data ) {
                    var msg = 'Deseja inativar o médico? <br/>';
                    $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                    $('#controle_confirma').val('I');
                    $('#cod_confirmado').val(cod);
                    $('#modalConfirma').modal('show');
                  },
                  error: function (){
                    $('.btn-controle').hide();  
                    $('.btn-fechar').show();                  
                    $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                    $('#modalConfirma').modal('show');
                  }
                });    
            }
            
        });


            //ativar medico
        $('.ativar_med').click(function(){
            var cod = $(this).attr('data-cod');

            console.log('inativar_med');

            if (cod!='' && cod!=null && cod!=undefined ) {

                $.ajax({
                  method: "POST",
                  url: "model/consulta_nome_medico.php",
                  data: { cod_medico: cod },
                  success: function( data ) {
                    var msg = 'Deseja Ativar o médico? <br/>';
                    $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                    $('#controle_confirma').val('A');
                    $('#cod_confirmado').val(cod);
                    $('#modalConfirma').modal('show');
                  },
                  error: function (){
                    $('.btn-controle').hide();  
                    $('.btn-fechar').show();                  
                    $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                    $('#modalConfirma').modal('show');
                  }
                });    
            }
            
        });

        //excluir medico
        $('.excluir_med').click(function(){
            var cod = $(this).attr('data-cod');
            console.log('excluir_med');

            if (cod!='' && cod!=null && cod!=undefined ) {

                $.ajax({
                  method: "POST",
                  url: "model/consulta_nome_medico.php",
                  data: { cod_medico: cod },
                  success: function( data ) {
                    var msg = 'Deseja excluir o médico? <br/>';
                    $('#msg_modal').css( "color", "#000000" ).html(msg+data);

                    $('#controle_confirma').val('E');
                    $('#cod_confirmado').val(cod);
                    $('#modalConfirma').modal('show');
                  },
                  error: function (){
                    $('.btn-controle').hide();    
                    $('.btn-fechar').show();                
                    $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                    $('#modalConfirma').modal('show');
                  }
                }); 
            }
            
        });

        $('.btn-confirma').click(function(){
            var cod = $('#cod_confirmado').val();
            var controle = $('#controle_confirma').val();

            if (cod!='' && cod!=null && cod!=undefined ) {
                console.log('btn-confirma');
                console.log(cod +' - '+controle );

                $.ajax({
                  method: "POST",
                  url: "model/grud_medico.php",
                  data: { id: cod, controle: controle },
                  success: function( data ) {

                    $('#msg_modal').css( "color", "#000000" ).html(data);
                    $('.btn-controle').hide(); 
                    $('.btn-fechar').show();  
                  },
                  error: function (){
                    $('.btn-controle').hide();  
                    $('.btn-fechar').show();                  
                    $('#msg_modal').css( "color", "red" ).html('<i class="glyphicon glyphicon-warning-sign"></i> <br/> Erro ao se conectar ao servidor.');
                    $('#modalConfirma').modal('show');
                  }
                }); 
            }

        });

        $('#modalConfirma').on('hidden.bs.modal', function (e) {
          
              $('#cod_confirmado').val('');
              $('#controle_confirma').val('');
              $('.btn-controle').show();
              $('.btn-fechar').hide();
              $('#msg_modal').css( "color", "#000000" ).html('');
              window.location.reload();
        })




    } );
    </script>


    <form id="form_enviar" method="post" accept-charset="UTF-8"  >
           <input type="hidden"  id="controle" name="controle"  value="">               
           <input type="hidden"  id="cod_medico" name="cod_medico"  value="">
    </form>


       


    <aside class="right-side">
        <section class="content">
            <h1>
                Lista de Médicos               
            </h1>
            <!-- start:breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-stethoscope"></i> Médico</a></li>
                <li class="active">Lista de médicos</li>
            </ol>
            <!-- end:breadcrumb -->

            <!-- start:content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box blank-page">
                        <div class="row">
                            <div class="col-md-12">
                                <section id="unseen">
                                    <table  class="display table table-bordered table-striped table-condensed" id="lista_medicos">
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>CRM</th>
                                                <th>CPF</th>
                                                <th>Celular</th>
                                                <th>Email</th>
                                                <th>Situação</th>
                                                <th>Configurações</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php while($row = mysqli_fetch_array($result)){ ?>
                                            <tr> <!-- class="danger" class="warning"-->
                                                <td><?php echo utf8_encode($row['nome']); ?></td>
                                                <td><?php echo utf8_encode($row['crm']); ?></td>
                                                <td><?php echo utf8_encode($row['cpf']); ?></td>
                                                <td><?php echo utf8_encode($row['celular']); ?></td>
                                                <td><?php echo utf8_encode($row['email']); ?></td>
                                                <td align="center"><?php  situacao($row['status']); ?></td>
                                                <td align="center">

                                                <!-- opções button -->
                                                <div class="btn-group">
                                                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                        Opções
                                                      <i class="fa fa-cog"></i> 
                                                      <span class="caret"></span>
                                                  </button>   
                                                      <ul class="dropdown-menu" role="menu" style="text-align: left;">
                                                        <li data-cod="<?php echo $row['cd_medico']; ?>" class="aletar_med"><a href="#"> <i class="fa fa-edit icon-circle icon-info"></i> Alterar </a></li>
                                                        <li data-cod="<?php echo $row['cd_medico']; ?>"  class="inativar_med"><a href="#"> <i class="fa fa-ban icon-circle icon-warning"></i> Inativar </a></li>
                                                        <li data-cod="<?php echo $row['cd_medico']; ?>"  class="ativar_med"><a href="#"> <i class="glyphicon glyphicon-ok icon-circle icon-success"></i> Ativar </a></li>                                                        
                                                        <li class="divider"></li>
                                                        <li data-cod="<?php echo $row['cd_medico']; ?>" class="excluir_med"><a href="#"> <i class="fa  fa-trash-o icon-circle icon-danger"></i> Excluir </a></li>
                                                      </ul>
                                                </div>
                                                <!-- end opções button -->
                                                    
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </section>
                            </div>                            
                        </div>





                    </div>
                </div>
            </div>
            <!-- end:content -->

        </section>
    </aside>
    <!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->


<!-- modal confirmação -->
    <div class="modal fade" id="modalConfirma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h4>
          </div>
          <div class="modal-body" align="center">
            <input type="hidden" id="cod_confirmado" value="" >   
            <input type="hidden" id="controle_confirma" value="" >
            <h2 id="msg_modal" style="color: #000000;">   </h2>
          </div>
          <div class="modal-footer"  align="right">
            <button type="button" class="btn btn-primary btn-confirma btn-controle" >Sim</button>
            <button type="button" class="btn btn-danger btn-controle" data-dismiss="modal">Não</button>
            <button type="button" class="btn btn-danger btn-fechar" style="display:none;"  data-dismiss="modal">Fechar</button>

          </div>
        </div>
      </div>
    </div>

<!-- end modal confirmação -->


</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Oct 2015 00:45:02 GMT -->
</html>