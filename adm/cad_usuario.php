<?php include 'head.php'; ?>
<?php include 'conexao/config.php'; ?>
<body class="cl-default fixed">

  <!-- inicio:navbar top -->
  <?php include 'head_menu_top.php'; ?>
  <!-- Fin:navbar top -->

  <!-- inicio: Toda Lateral do menu -->
  <?php include 'head_menu_left.php'; ?>
  <!-- fim: Toda Lateral do menu -->

  <?php 

  $query = "SELECT * FROM medico WHERE status = 'A'";
  $result = mysqli_query($conn, $query);
  $total_num_rows = mysqli_num_rows($result);
  ?>

  <style>
  .form-group{
    padding-left:10px;
    padding-right: 10px;
  }
  </style>
<script>
  var url_img_default = 'img/user/avatar-<?php echo(rand(1,5)); ?>.jpg';
</script>
<script src="js/script-cadastro-usuario.js"></script>


<link rel="stylesheet" href="plugins/file-uploader/css/jquery.fileupload.css">



<aside class="right-side">
  <section class="content">
    <h1>
      Cadastro Usuário
    </h1>
    <!-- start:breadcrumb -->
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-user"></i> Usuário</a></li>
      <li class="active">Novo Usuário</li>
    </ol>
    <!-- end:breadcrumb -->

    <!-- start:content -->
    <div class="row">
      <div class="col-md-12">
        <div class="box blank-page">
        
        <!-- Controle de pesquisa de medico -->
        <div id="c_medico"></div>

         
            <fieldset>
              <!-- Form Name -->
              <legend>Usuário</legend>

              <div class="row">
                <div class="col-md-10">

                  <section class="panel">

                    <div class="panel-body">
                      <form class="form-horizontal" role="form" id="form_cad_usr" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="controle" value="N">

                        <div class="row">
                          <div class="col-md-6">

                            <div class="form-group">
                              <label class="col-md-4 control-label" for="vinculo_user">Vinculo Médico</label>
                              <div class="col-md-4">
                                <div class="radio">
                                  <label for="vinculo_user-1">
                                    <input type="radio" name="vinculo_user" id="vinculo_user-1" value="1" >
                                    Sim
                                  </label>
                                </div>
                                <div class="radio">
                                  <label for="vinculo_user-2">
                                    <input type="radio" name="vinculo_user" id="vinculo_user-2" value="2"  checked>
                                    Não
                                  </label>
                                </div>
                              </div>
                            </div>

                          </div>
                          <div class="col-md-6" >
                            <div class="form-group" id="listaMedico">
                              <label for="estado">Médico</label>
                              <select class="form-control m-bot15" id="vinculoMedico" name="id_medico"  required>
                                <option value="">Selecione o médico</option>
                                <?php while($row = mysqli_fetch_array($result)){ ?>
                                <option value="<?php echo utf8_encode($row['cd_medico']); ?>">
                                  <?php echo utf8_encode($row['nome']); ?>
                                </option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                        </div>


                        <div class="form-group">
                          <label for="exampleInputEmail1">Nome</label>
                          <input type="text" class="form-control" id="nome_user" name="nome_user" placeholder="Digite o nome" required>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Login</label>
                              <input type="text" class="form-control" id="login_user" name="login_user" placeholder="Digite o login" required>
                            </div>
                          </div>
                          <div class="col-md-6">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Senha</label>
                              <input type="password" class="form-control" id="senha_user" name="senha_user" placeholder="Digite a senha"  required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Confirme Senha</label>
                              <input type="password" class="form-control" id="confirme_senha_user" name="confirme_senha_user"  placeholder="Digite novamente a senha"  required>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Email</label>
                              <input type="email" class="form-control" id="email_user" name="email_user" placeholder="Digite o email"  required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Homepage</label>
                              <input type="text" class="form-control" id="homepage_user" name="homepage_user" placeholder="Digite o site, blog e etc...">
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Telefone</label>
                              <input type="text" class="form-control telefone_user" id="telefone_user" name="telefone_user" placeholder="Digite o telefone">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleInputPassword1">Celular</label>
                              <input type="text" class="form-control celular_user" id="celular_user" name="celular_user" placeholder="Digite o celular"  required>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="cep" >CEP</label>
                              <input type="text" class="form-control cep_user" id="cep_user" name="cep_user" placeholder="Digite o cep" required>
                            </div>
                          </div>
                          <div class="col-md-6">

                            <div class="form-group">
                              <label for="estado"> Estado</label>
                              <select class="form-control m-bot15" id="estado_user" name="estado_user" required>
                                <option value="">Selecione o estado</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espirito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="endereco">Endereço</label>
                          <input type="text" class="form-control" id="endereco_user" name="endereco_user" placeholder="Digite o endereço" required>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="bairro">Bairro</label>
                              <input type="text" class="form-control" id="bairro_user" name="bairro_user" placeholder="Digite o bairro" required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="cidade">Cidade</label>
                              <input type="text" class="form-control" id="cidade_user" name="cidade_user" placeholder="Digite a cidade" required>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="estado">Usuário faz solicitação de exame ?</label>
                              <select class="form-control m-bot15" id="solicitacao_user" name="solicitacao_user" required>
                                <option value="">Selecione</option>
                                <option value="S">Sim</option>
                                <option value="N">Não</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="estado">Perfil</label>
                              <select class="form-control m-bot15" id="perfil_user" name="perfil_user" required>
                                <option value="">Selecione o perfil</option>
                                <option value="1">Administrador</option>
                                <option value="2">Colaborador</option>
                                <option value="3">Usuário solicitante</option>
                                <option value="3">Usuário executante</option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6" align="right">
                            <div class="form-group">

                             <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Adicionar foto</span>
                              <input id="file_imagem"  name="file_imagem" type="file"   />
                            </span>

                          </div>
                        </div>
                        <div class="col-md-6">
                          <img id="uploadPreview1" src="img/user/avatar-<?php echo(rand(1,5)); ?>.jpg" width="150" height="150" />
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="descricao">Observação</label>
                        <textarea class="form-control" id="descricao" name="observacao_user" maxlength="500" rows="5"></textarea>
                      </div>

                      <br>

                      <div align="right">
                        <button class="btn btn-danger btn_reset">Cancelar</button>
                        <button type="submit" class="btn btn-success">Salvar</button>
                      </div>


                 
                  </div>
                </section>


              </div>
            </div>

          </fieldset>
        </form>





      </div>
    </div>
  </div>
  <!-- end:content -->

</section>
</aside>
<!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->


  <!-- Modal -->
  <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h4>
        </div>
        <div class="modal-body" align="center">
          <h1 id="msg_modal" style="color: #2ECC71;">   </h1>
        </div>
        <div class="modal-footer"  align="right">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>

        </div>
      </div>
    </div>
  </div>
  <!-- end modal  -->



</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Oct 2015 00:45:02 GMT -->
</html>