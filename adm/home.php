<?php include 'head.php'; ?>
<?php include 'conexao/config.php' ?>
<?php date_default_timezone_set('America/Sao_Paulo'); ?>

<body class="cl-default fixed">

    <!-- inicio:navbar top -->
    <?php include 'head_menu_top.php'; ?>
    <!-- Fin:navbar top -->
    
    <br>
    <!-- inicio: Toda Lateral do menu -parametro($vMenu)->
    <?php include 'head_menu_left.php'; ?>
    <!-- fim: Toda Lateral do menu -->

    <link href='plugins/fullcalendar-2.0.2/fullcalendar.css' rel='stylesheet' />
    <link href='plugins/fullcalendar-2.0.2/fullcalendar.print.css' rel='stylesheet' media='print' />

    <!-- start:javascript for this page -->
    <script src='plugins/fullcalendar-2.0.2/lib/moment.min.js'></script>
    <script src='plugins/fullcalendar-2.0.2/lib/jquery-ui.custom.min.js'></script>
    <script src='plugins/fullcalendar-2.0.2/fullcalendar.min.js'></script>
    <script src='plugins/fullcalendar-2.0.2/lang-all.js'></script>

    <?php 

    $query_agendados = "SELECT COUNT(*) FROM solicitacao WHERE status = 'A'";
    $result_agendados = mysqli_query($conn, $query_agendados);
    $row_agendados = mysqli_fetch_array($result_agendados);


    $query_pendentes = "SELECT COUNT(*) FROM solicitacao WHERE status = 'AC'";
    $result_pendentes = mysqli_query($conn, $query_pendentes);
    $row_pendentes = mysqli_fetch_array($result_pendentes);

    $query_excluidos = "SELECT COUNT(*) FROM solicitacao WHERE status = 'E'";
    $result_excluidos = mysqli_query($conn, $query_excluidos);
    $row_excluidos = mysqli_fetch_array($result_excluidos);

    $query = "SELECT 
    nm_paciente,
    DATE_FORMAT(dt_solicitacao, '%d/%m/%Y %H:%i:%s') as dt_solicitacao,
    status
    FROM solicitacao order by cd_solicitacao desc ";
    $result = mysqli_query($conn, $query);


    function converterData($dataSolic){
        $dataString = $dataSolic;
        $date = DateTime::createFromFormat('d/m/Y H:i:s', $dataString);
        $dataEUA =  $date->format('Y-m-d H:i:s');
        echo "'".$dataEUA."'";

    }

    function colorCalendar($arges){
        switch ($arges) {
            case 'A':
            $color = '#2ECC71';
            break;
            case 'I':
            $color = '#F1C40F';
            break;
            case 'E':
            $color = '#C0392B';
            break;
            case 'AC':
            $color = '#3498DB';
            break;
            default:
            $color = '';
            break;
        }

        echo "'".$color."'";

    }
    ?>
    <aside class="right-side">
        <section class="content">
            <h1>
                Bem Vindo
                <small>Sistema de controle de agendamento</small>
            </h1>
            <!-- start:breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Painel de Controle</li>
            </ol>
            <!-- end:breadcrumb -->

            <!-- start:content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box blank-page">
                        <br>
                        <div class="row" align="center">
                          <div class="col-md-4">

                              <div class="the-box bg-success tiles-information">
                                <i class="fa fa-user-md icon-bg"></i>
                                <div class="tiles-inner text-center">
                                    <h3><b>AGENDADOS</b></h3>
                                    <h1 class="bolded"><?php echo $row_agendados['COUNT(*)']; ?></h1> 
                                    <!--  <p><small>Better than yesterday ( 2,5% )</small></p> -->
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="the-box bg-info tiles-information">
                                <i class="fa fa-stethoscope  icon-bg"></i>
                                <div class="tiles-inner text-center">
                                    <h3><b>PENDENTES</b></h3>
                                    <h1 class="bolded"><?php echo $row_pendentes['COUNT(*)']; ?></h1>
                                    <!--  <p><small>Better than yesterday ( 13,5% )</small></p> -->
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="the-box bg-danger tiles-information">
                                <i class="fa fa-medkit icon-bg"></i>
                                <div class="tiles-inner text-center">
                                    <h3><b>CANCELADOS</b></h3>
                                    <h1 class="bolded"><?php echo $row_excluidos['COUNT(*)']; ?></h1> 
                                    <!--  <p><small>Less than yesterday ( <span class="text-danger">-7,5%</span> )</small></p> -->
                                </div>
                            </div>

                        </div>


                    </div>
                    <h3>AGENDAMENTOS COM PENDENCIAS</h3>
                    <hr>
                        <!-- start:content -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- start:basic calendar -->
                                <div class="box">
                                 <div id='calendar'></div>
                             </div>
                             <!-- end:basic calendar -->
                         </div>
                     </div>
                     <!-- end:content -->
                     <script>

                     $(document).ready(function() {
                        var currentLangCode = 'pt-br';

            // build the language selector's options
            $.each($.fullCalendar.langs, function(langCode) {
                $('#lang-selector').append(
                    $('<option/>')
                    .attr('value', langCode)
                    .prop('selected', langCode == currentLangCode)
                    .text(langCode)
                    );
            });

            // rerender the calendar when the selected option changes
            $('#lang-selector').on('change', function() {
                if (this.value) {
                    currentLangCode = this.value;
                    $('#calendar').fullCalendar('destroy');
                    renderCalendar();
                }
            });

            function renderCalendar() {
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    defaultDate:<?php echo "'". date("2016-m-d") ."'";?>,
                    lang: currentLangCode,
                    buttonIcons: false, // show the prev/next text
                    weekNumbers: false,
                    editable: false,
                    events: [
                    <?php 
                    while($row = mysqli_fetch_array($result)){?>
                        {
                           title: <?php echo "'". $row['nm_paciente'] ."'";?>,
                           start: <?php echo converterData($row['dt_solicitacao']);?>,
                           color: <?php echo colorCalendar($row['status']);?>
                       },
                       <?php } ?>

                       ]
                   });
}

renderCalendar();
});

</script>

</div>
</div>
</div>
<!-- end:content -->



</section>
</aside>
<!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->



</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Oct 2015 00:45:02 GMT -->
</html>