<?php include 'head.php'; ?>
<body class="cl-default fixed">

    <!-- inicio:navbar top -->
    <?php include 'head_menu_top.php'; ?>
    <!-- Fin:navbar top -->
    

    <!-- inicio: Toda Lateral do menu -->
    <?php include 'head_menu_left.php'; ?>
    <!-- fim: Toda Lateral do menu -->




    <style>
    .form-group{
        padding-left:10px;
        padding-right: 10px;
    }
    </style>


    <?php 

    $queryMed = "SELECT *
                 FROM medico 
                 order by nome ";
    $resultMed = mysqli_query($conn, $queryMed);

    if (isset($_POST['data_hora_inicial'])) {
        $mascaraIni = $_POST['data_hora_inicial'];
        $data_hora_inicial =  $_POST['data_hora_inicial'];
    }else{
        $mascaraIni = "";
        $data_hora_inicial = 'NOW()';
    }

    if (isset($_POST['data_hora_final'])) {
        $mascaraFim =  $_POST['data_hora_final'];
        $data_hora_final =  $_POST['data_hora_final'];
    }else{
        $mascaraFim = "";
        $data_hora_final = 'NOW()';
    }

    if (isset($_POST['cd_medico'])) {
        $cd_medico =  $_POST['cd_medico'];
    }else{
        $cd_medico = '%';
    }

    $query = "SELECT s.dt_solicitacao, m.nome, s.nm_paciente
              FROM solicitacao s,
                   medico m
              where cd_medico_solicitante = cd_medico 
              and s.dt_solicitacao >= '$data_hora_inicial'
              and s.dt_solicitacao <= '$data_hora_final'";
    $result = mysqli_query($conn, $query);
    $total_num_rows = mysqli_num_rows($result);


    ?>

    <script>
    $(function(){

        $('#dataInicial').datetimepicker({
            language:'pt-br', 
            pickTime: true                          
        });

        $('#dataFinal').datetimepicker({
            language:'pt-br', 
            pickTime: true                          
        });


    });
    </script>
    <aside class="right-side">
        <section class="content">
            <h1>
                Médico por Agenda
            </h1>
            <!-- start:breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-file-text-o"></i> Relatório</a></li>
                <li class="active">Médico por agenda</li>
            </ol>
            <!-- end:breadcrumb -->

            <!-- start:content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box blank-page">


                        <form class="form-horizontal tasi-form" id="form_exame" method="post"
                        accept-charset="UTF-8" enctype="application/x-www-form-urlencoded"
                        autocomplete="off" >

                        <input type="hidden" name="controle" id="controle" value="N">
                        <fieldset>
                            <!-- Form Name -->
                            <legend>Consulta</legend>

                            <div class="row">
                                <div class="col-md-12">

                                  <section class="panel">

                                    <div class="panel-body">
                                        <form action="r_agenda_medico.php" class="form-horizontal" role="form" method="get">                                           

                                            <div class="row">
                                              <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="valor">Data - Hora inicial</label>
                                                    <input type="text" id="dataInicial" name="data_hora_inicial" class="form-control" value="<?php echo $mascaraIni ?>" required>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="valor">Data - Hora final</label>
                                                    <input type="text" id="dataFinal" name="data_hora_final" class="form-control" value="<?php echo $mascaraFim ?>" required>
                                                </div>
                                            </div>


                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="valor">Médico</label>
                                                    <select class="form-control" name="nome_medico"  required>
                                                        <option value="">Selecione o médico</option>  
                                                        <?php while($row = mysqli_fetch_array($resultMed)){ ?>                                                      
                                                        <option value="<?php echo utf8_encode($row['cd_medico']); ?>">
                                                            <?php echo utf8_encode($row['nome']); ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div style="    margin-top: 25px;">
                                                <button class="btn btn-info">
                                                    Consultar &nbsp;<i class="fa fa-search"></i>
                                                </button> &nbsp; &nbsp;
                                                <button  class="btn btn-cascade">
                                                    Imprimir  &nbsp;<i class="fa fa-print"></i>
                                                </button>
                                            </div>

                                        </div>
                                        <br><br>

                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Data - Hora</th>
                                                    <th>Médico</th>
                                                    <th>Paciente</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php  $cont=1; 
                                                    while($row = mysqli_fetch_array($result)){
                                                ?>
                                                <tr>
                                                    <td><b><?php echo $cont; ?></b></td>
                                                    <td><?php echo $row['dt_solicitacao']; ?></td>
                                                    <td><?php echo utf8_encode($row['nome']); ?></td>
                                                    <td><?php echo utf8_encode($row['nm_paciente']); ?></td>
                                                </tr>
                                                <?php $cont++; } ?>
                                            </tbody>
                                        </table>





                                    </form>

                                </div>
                            </section>


                        </div>
                    </div>

                </fieldset>
            </form>





        </div>
    </div>
</div>
<!-- end:content -->

</section>
</aside>
<!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->


<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h4>
            </div>
            <div class="modal-body" align="center">
                <h1 id="msg_modal" style="color: #2ECC71;">   </h1>
            </div>
            <div class="modal-footer"  align="right">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>
<!-- end modal  -->



</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Oct 2015 00:45:02 GMT -->
</html>