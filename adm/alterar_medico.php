<?php include 'head.php'; ?>
<body class="cl-default fixed">

    <!-- inicio:navbar top -->
    <?php include 'head_menu_top.php'; ?>
    <!-- Fin:navbar top -->

    <!-- inicio: Toda Lateral do menu -->
    <?php include 'head_menu_left.php'; ?>
    <!-- fim: Toda Lateral do menu -->


    <?php include 'model/consulta_medico_alterar.php'; ?>

    <script src="js/script-alterar-medico.js"></script> 

    <style>
    .form-group{
        padding-left: 10px;
        padding-right: 10px;
    }
    </style>

    <aside class="right-side">
        <section class="content">
            <h1>
                Alterar cadastro Médico
            </h1>
            <!-- start:breadcrumb -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-stethoscope"></i> Médico</a></li>
                <li class="active">Alterar médico</li>
            </ol>
            <!-- end:breadcrumb -->

            <!-- start:content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box blank-page">

                        <form class="form-horizontal tasi-form" id="form_medico"
                          method="post" action="grud_medico.php" accept-charset="UTF-8"
                          enctype="application/x-www-form-urlencoded"  autocomplete="off" >
                          <input type="hidden" name="controle" value="M">
                          <input type="hidden" name="id" value="<?php echo utf8_encode($row['cd_medico']); ?>">

                            <fieldset>
                                <!-- Form Name -->
                                <legend>Dados do perfil</legend>

                                <div class="row">
                                    <div class="col-md-10">

                                      <section class="panel">

                                        <div class="panel-body">
                                            <form class="form-horizontal" role="form">
                                                <div class="form-group">
                                                    <label for="nome">Nome</label>
                                                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o nome do médico"  value="<?php echo utf8_encode($row['nome']);?>" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="especialidade">Especialidade Médica</label>
                                                    <input type="text" class="form-control" id="especialidade" name="especialidade" placeholder="Digite a especialidade médica" value="<?php echo utf8_encode($row['especialidade']);?>" required>
                                                </div>

                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="crm">CRM</label>
                                                        <input type="text" class="form-control" id="crm" name="crm" placeholder=" Digite o CRM do médico " value="<?php echo utf8_encode($row['crm']); ?>" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="cpf">CPF</label>
                                                        <input type="text" class="form-control cpf"  id="cpf" name="cpf"  maxlength="14" placeholder="Digite o CPF" value="<?php echo utf8_encode($row['cpf']);?>" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="telefone">Telefone</label>
                                                    <input type="text" class="form-control telefone" id="telefone" name="telefone" placeholder=" Digite o telefone" value="<?php echo utf8_encode($row['telefone']);?>" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="celular">Celular</label>
                                                    <input type="text" class="form-control celular" id="celular" name="celular" placeholder=" Digite o celular" value="<?php echo utf8_encode($row['celular']);?>" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Digite o email." value="<?php echo utf8_encode($row['email']);?>" required>
                                        </div>

                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="cep" >CEP</label>
                                                <input type="text" class="form-control cep" id="cep" name="cep" placeholder="Digite o cep" value="<?php echo utf8_encode($row['cep']);?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                
                                            <div class="form-group">
                                                <label for="estado">Estado</label>
                                                <select class="form-control m-bot15" id="estado" name="estado" required>
                                                  <option value="">Selecione o estado</option>
                                                  <option value="AC" <?=$row['estado'] == 'AC' ? ' selected="selected"' : '';?> >Acre</option>
                                                  <option value="AL" <?=$row['estado'] == 'AL' ? ' selected="selected"' : '';?> >Alagoas</option>
                                                  <option value="AP" <?=$row['estado'] == 'AP' ? ' selected="selected"' : '';?> >Amapá</option>
                                                  <option value="AM" <?=$row['estado'] == 'AM' ? ' selected="selected"' : '';?> >Amazonas</option>
                                                  <option value="BA" <?=$row['estado'] == 'BA' ? ' selected="selected"' : '';?> >Bahia</option>
                                                  <option value="CE" <?=$row['estado'] == 'CE' ? ' selected="selected"' : '';?> >Ceará</option>
                                                  <option value="DF" <?=$row['estado'] == 'DF' ? ' selected="selected"' : '';?> >Distrito Federal</option>
                                                  <option value="ES" <?=$row['estado'] == 'ES' ? ' selected="selected"' : '';?> >Espirito Santo</option>
                                                  <option value="GO" <?=$row['estado'] == 'GO' ? ' selected="selected"' : '';?> >Goiás</option>
                                                  <option value="MA" <?=$row['estado'] == 'MA' ? ' selected="selected"' : '';?> >Maranhão</option>
                                                  <option value="MS" <?=$row['estado'] == 'MS' ? ' selected="selected"' : '';?> >Mato Grosso do Sul</option>
                                                  <option value="MT" <?=$row['estado'] == 'MT' ? ' selected="selected"' : '';?> >Mato Grosso</option>
                                                  <option value="MG" <?=$row['estado'] == 'MG' ? ' selected="selected"' : '';?> >Minas Gerais</option>
                                                  <option value="PA" <?=$row['estado'] == 'PA' ? ' selected="selected"' : '';?> >Pará</option>
                                                  <option value="PB" <?=$row['estado'] == 'PB' ? ' selected="selected"' : '';?> >Paraíba</option>
                                                  <option value="PR" <?=$row['estado'] == 'PR' ? ' selected="selected"' : '';?> >Paraná</option>
                                                  <option value="PE" <?=$row['estado'] == 'PE' ? ' selected="selected"' : '';?> >Pernambuco</option>
                                                  <option value="PI" <?=$row['estado'] == 'PI' ? ' selected="selected"' : '';?> >Piauí</option>
                                                  <option value="RJ" <?=$row['estado'] == 'RJ' ? ' selected="selected"' : '';?> >Rio de Janeiro</option>
                                                  <option value="RN" <?=$row['estado'] == 'RN' ? ' selected="selected"' : '';?> >Rio Grande do Norte</option>
                                                  <option value="RS" <?=$row['estado'] == 'RS' ? ' selected="selected"' : '';?> >Rio Grande do Sul</option>
                                                  <option value="RO" <?=$row['estado'] == 'RO' ? ' selected="selected"' : '';?> >Rondônia</option>
                                                  <option value="RR" <?=$row['estado'] == 'RR' ? ' selected="selected"' : '';?> >Roraima</option>
                                                  <option value="SC" <?=$row['estado'] == 'SC' ? ' selected="selected"' : '';?> >Santa Catarina</option>
                                                  <option value="SP" <?=$row['estado'] == 'SP' ? ' selected="selected" ' : '';?> >São Paulo</option>
                                                  <option value="SE" <?=$row['estado'] == 'SE' ? ' selected="selected" ' : '';?> >Sergipe</option>
                                                  <option value="TO" <?=$row['estado'] == 'TO' ? ' selected="selected" ' : '';?> >Tocantins</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="endereco">Endereço</label>
                                        <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Digite o endereço" value="<?php echo utf8_encode($row['endereco']);?>" required>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="bairro">Bairro</label>
                                            <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Digite o bairro" value="<?php echo utf8_encode($row['bairro']);?>" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="cidade">Cidade</label>
                                            <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Digite a cidade" value="<?php echo utf8_encode($row['cidade']);?>" required>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="banco">Banco</label>
                                    <input type="text" class="form-control" id="banco" name="banco" placeholder="Digite o nome do banco"   value=" <?php echo utf8_encode($row['banco']);?>"  required>
                                </div>

                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="agencia">Agência</label>
                                        <input type="text" class="form-control" id="agencia" name="agencia" placeholder="Digite o número da agência" value="<?php echo utf8_encode($row['agencia']);?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="conta">Conta</label>
                                        <input type="text" class="form-control" id="conta" name="conta" placeholder="Digite o número da conta" value="<?php echo utf8_encode($row['conta']);?>" required>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div align="right">
                                <button class="btn btn-danger btn_reset">Cancelar</button>
                                <button type="submit" class="btn btn-success">Salvar alterações</button>
                            </div>




                        </form>

                    </div>
                </section>


            </div>
        </div>

    </fieldset>
</form>



</div>
</div>
</div>
<!-- end:content -->

</section>
</aside>
<!-- end:right sidebar -->

</div>
<!-- end:wrapper body -->



<!-- modal  -->

<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"> &nbsp; &nbsp; </h4>
      </div>
      <div class="modal-body" align="center">
        <h1 id="msg_modal" style="color: #2ECC71;">   </h1>
      </div>
      <div class="modal-footer"  align="right">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>

      </div>
    </div>
  </div>
</div>

<!-- end modal  -->






</body>

</html>
