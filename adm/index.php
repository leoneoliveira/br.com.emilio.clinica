<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<title>Clinica </title>
	<!-- start:bootstrap v3.2.0 -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<!-- start:font awesome v4.1.0 -->
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<!-- start:bootstrap reset -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap-reset.css">
	<!-- start:style arjuna -->
	<link rel="stylesheet" type="text/css" href="css/arjuna.css">


	<script src="js/moment-with-locales.js" ></script>
	<script type="text/javascript">
	moment.locale('pt-br');
	</script>

	<!-- start:javascript for all pages -->
	<!-- start:jquery -->
	<script src="js/jquery-1.11.1.min.js"></script>
	<!-- start:bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- start:arjuna.js -->
	<script src="js/arjuna.js"></script>

</head>
<body class="cl-default fixed" id="login">

	<?php 
	if (isset($_GET['erro'])) {
		$erro = $_GET['erro'];
	}else{
		$erro = '';
	}
	?>
	<!-- start:wrapper -->
	<div class="header-login">
		<div class="text-center">
			<h1>Sistema de controle de agendamento</h1>
		</div>
	</div>

	<div class="body-login">
		<?php if ($erro ==  1): ?>
		<div class="alert alert-dismissible alert-danger alert-login" role="alert">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;
			</span><span class="sr-only">Close</span></button>
			<strong>Atenção!</strong> Login ou Senha invalido, por favor verificar e logar novamente.
		</div>
	<?php endif ?>

	<form role="form" action="autenticar.php" method="post">
		<div class="form-group">
			<input type="text" name="login" class="form-control input-lg" id="emil" placeholder="Entre com login">
		</div>
		<div class="form-group">
			<input type="password" name="senha" class="form-control input-lg" id="password" placeholder="Entre a senha">
		</div>
		<div class="checkbox">
			<label>
				<input type="checkbox"> Mantenha-me conectado
			</label>
		</div>
		<button type="submit" class="btn btn-default btn-block btn-lg btn-perspective">LOGIN</button>
	</form>
</div>	
</body>
<html>