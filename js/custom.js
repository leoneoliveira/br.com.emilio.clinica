$('.my-tooltip').tooltip();
$('.header').waypoint('sticky');

/* *************************************** */ 
/* Scroll to Top */
/* *************************************** */  
		
$(document).ready(function() {

	var lastId, topMenuHeight = $("#top-menu").outerHeight();
	scrollItems = $("#top-menu").find("a").map(function(){
									if ($(this).attr("href").length) { return $(this).attr("href"); }
								});

	$("#top-menu").find("a").click(function(e)
	{
		var href = $(this).attr("href"),
		offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
		$('html, body').stop().animate({scrollTop: offsetTop}, 1000,'easeInOutExpo');
		e.preventDefault();
	});

	$(".totop").hide();
	
	$(window).scroll(function(){

	var fromTop = $(this).scrollTop()+topMenuHeight;
	var cur = scrollItems.map(function()
	{
		if ($(this).offset().top < fromTop)
		return this;
	});
	cur = cur[cur.length-1];
	var id = cur && cur.length ? cur[0].id : "";
	if (lastId !== id)
	{
		lastId = id;
		$("#top-menu").find("a").parent().removeClass("active")
		.end().filter("[href=#"+id+"]").parent().addClass("active");
	}



	if ($(this).scrollTop() > 300) {
		$('.totop').fadeIn();
	} else {
		$('.totop').fadeOut();
	}
	});
	$(".totop a").click(function(e) {
		e.preventDefault();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});
		
});


var firstLoad = true;
$('.skills-1').waypoint(function(){
	if(firstLoad){
		$('.countTo').each(count);
		firstLoad = false;
	}
	function count(options) {
		var $this = $(this);
		options = $.extend({}, options || {}, $this.data('countToOptions') || {});
		$this.countTo(options);
	  }
	  
	$('.skill-chart').each(function(){
		$(this).easyPieChart({
				size:140,
				animate: 2000,
				scaleColor: false,
				barColor: '#fff',
				trackColor: 'transparent',
				lineWidth: 10
		});
	});					
	
},{offset:'80%'});
