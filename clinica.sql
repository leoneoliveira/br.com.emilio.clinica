-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Máquina: localhost:3306
-- Data de Criação: 11-Abr-2016 às 22:18
-- Versão do servidor: 5.5.45-cll-lve
-- versão do PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `memedbr_clina_producao`
--
--
-- Base de Dados: `memedbr_clinica_homologacao`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `exame`
--

CREATE TABLE IF NOT EXISTS `exame` (
  `cd_exame` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `descricao` varchar(1000) DEFAULT NULL,
  `valor` double(15,2) DEFAULT NULL,
  `rateio` int(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `user_cad` varchar(100) NOT NULL,
  `user_alt` varchar(100) DEFAULT NULL,
  `dt_cad` date NOT NULL,
  `dt_alt` date DEFAULT NULL,
  PRIMARY KEY (`cd_exame`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Extraindo dados da tabela `exame`
--

INSERT INTO `exame` (`cd_exame`, `nome`, `descricao`, `valor`, `rateio`, `status`, `user_cad`, `user_alt`, `dt_cad`, `dt_alt`) VALUES
(9, 'Abdominal total', 'Paciente em jejum de no minimo 4 h. Se o exame for marcado pela manha o paciente deve ser orientado a não tomar cafe da manha. Se o exame for marcado para a tarde, o paciente deve ser orientado a não almocar.', 100.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(10, 'Abdomen superior', 'Paciente em jejum de no minimo 4 h. Se o exame for marcado pela manha o paciente deve ser orientado a não tomar cafe da manha. Se o exame for marcado para a tarde, o paciente deve ser orientado a não almocar.', 80.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(11, 'Abdomen inferior', 'O preparo do paciente sera com 4 copos de agua 30 minutos antes da realização do exame', 80.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(12, 'Prostata', 'O preparo do paciente sera com 4 copos de agua 30 minutos antes da realização do exame', 90.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(13, 'Pelvico', 'O preparo do paciente sera com 4 copos de agua 30 minutos antes da realização do exame', 80.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(14, 'Transvaginal ( endovaginal )', 'Limitacoes para o exame : paciente virgem.', 90.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(15, 'Mamas e axilas', 'Se possivel, trazer mamografia', 90.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(16, 'Vias biliares', 'Preparo para o exame: jejum de 6 h', 80.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(17, 'Vias urinarias', 'O preparo do paciente sera com 4 copos de agua 30 minutos antes da realização do exame', 80.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(18, 'Tiroide', 'O exame é realizado com avaliação por dopplerfluxometria', 90.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(19, 'Testiculo', 'O exame é realizado com avaliação por dopplerfluxometria', 90.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(20, 'Doppler venoso de membros inferiores', 'O valor é referente a cada membro ( cada perna )', 150.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(21, 'Articulações ( ombro, cotovelo, punhos, maos, quadril, joelho, tornozelo, pé )', 'Valor referente a cada lado', 90.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(22, 'Pequenas partes', 'Pequenos caroços, partes diferentes de articulações', 90.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(23, 'Doppler arterial de membros inferiores', 'Valor referente a cada membro', 150.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(24, 'Doppler venoso de membro superior', 'Valor referente a cada membro', 150.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(25, 'Doppler arterial de membro superior', 'Valor referente a cada membro', 150.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(26, 'Abdominal total com doppler', 'Preparo: jejum de 6h', 300.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(27, 'Doppler renal', 'Preparo: jejum de 6 h', 150.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(28, 'Obstetrico', 'Trazer exames anteriores', 80.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(29, 'Obstetrico com doppler', '>32 semanas', 100.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(30, 'Morfologico 1o. trimestre', 'Periodo avaliado da gestação de 11 a 14 semanas', 180.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(31, 'Morfologico 2o.trimestre', 'Exame realizado no intervalo de 18 a 28 semanas da gestação, sendo a melhor data a de 22 semanas.', 180.00, 100, 'A', '10', '10', '2016-02-09', '2016-02-09'),
(32, 'Doppler de carotidas', 'Requer solicitação', 150.00, 100, 'A', '10', NULL, '2016-02-09', NULL),
(33, 'Doppler de carotidas e vertebrais', 'Requer solicitação', 180.00, 100, 'A', '10', NULL, '2016-02-09', NULL),
(34, 'Parede abdominal', 'Exame realizado para investigar hernias', 80.00, 100, 'A', '10', NULL, '2016-02-09', NULL),
(35, 'Obstetrico 4D', 'Exame realizado para obtenção de imagens fotograficas do feto', 200.00, 100, 'A', '10', NULL, '2016-02-09', NULL),
(36, 'Região cervical', 'avaliação do pescoço', 100.00, 100, 'A', '10', NULL, '2016-02-09', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `medico`
--

CREATE TABLE IF NOT EXISTS `medico` (
  `cd_medico` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `crm` varchar(50) NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `telefone` varchar(30) NOT NULL,
  `celular` varchar(30) NOT NULL,
  `email` varchar(200) NOT NULL,
  `cep` varchar(50) NOT NULL,
  `endereco` varchar(150) NOT NULL,
  `bairro` varchar(150) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `banco` varchar(150) NOT NULL,
  `agencia` varchar(50) NOT NULL,
  `conta` varchar(50) NOT NULL,
  `user_cad` varchar(20) NOT NULL,
  `user_alt` varchar(20) NOT NULL,
  `dt_cad` date NOT NULL,
  `dt_alt` date NOT NULL,
  `status` varchar(10) NOT NULL,
  `especialidade` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cd_medico`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `medico`
--

INSERT INTO `medico` (`cd_medico`, `nome`, `crm`, `cpf`, `telefone`, `celular`, `email`, `cep`, `endereco`, `bairro`, `cidade`, `estado`, `banco`, `agencia`, `conta`, `user_cad`, `user_alt`, `dt_cad`, `dt_alt`, `status`, `especialidade`) VALUES
(12, 'Micheline Ebrahim', '13524-PE', '032.524.944-00', '(81) 9961-8488', '(81) 99618-488', 'micheline@me.med.br', '54783-010', 'Estrada de Aldeia 11971 Condominio Torquato Castro Rua Jatoba 170 ', 'Aldeia', 'Camaragibe', 'PE', 'Bradesco', '99999', '99999', '9', '', '2016-01-27', '0000-00-00', 'A', 'Ultrassonografista'),
(13, 'Clinica Jordao', '0080', '032.524.944-00', '(81) 3343-3077', '(81) 96184-883', 'lourdesgurgel@hotmail.com', '54315-320', 'jordao baixo', 'jordao', 'recife', 'PE', 'bradesco', '0231', '7893', '10', '', '2016-02-09', '0000-00-00', 'A', 'Clinica medica'),
(14, 'Bem estar', '13524', '032.524.944-00', '(81) 3428-6965', '(81) 34286-965', 'bemestar@hotmail.com', '50770-720', 'rua sao miguel, 630', 'afogados', 'recife', 'PE', 'bradesco', '0231', '7938', '10', '', '2016-02-09', '0000-00-00', 'A', 'clinica medica');

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacao`
--

CREATE TABLE IF NOT EXISTS `solicitacao` (
  `cd_solicitacao` int(10) NOT NULL AUTO_INCREMENT,
  `cd_medico_solicitante` int(100) NOT NULL,
  `cd_medico_executante` int(100) NOT NULL,
  `nm_paciente` varchar(100) NOT NULL,
  `dt_solicitacao` datetime NOT NULL,
  `cpf_paciente` varchar(100) NOT NULL,
  `telefone` varchar(100) NOT NULL,
  `celular` varchar(100) NOT NULL,
  `exames` varchar(100) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `user_cad` varchar(100) NOT NULL,
  `user_alt` varchar(100) NOT NULL,
  `dt_cad` date NOT NULL,
  `dt_alt` date NOT NULL,
  PRIMARY KEY (`cd_solicitacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Extraindo dados da tabela `solicitacao`
--

INSERT INTO `solicitacao` (`cd_solicitacao`, `cd_medico_solicitante`, `cd_medico_executante`, `nm_paciente`, `dt_solicitacao`, `cpf_paciente`, `telefone`, `celular`, `exames`, `descricao`, `status`, `user_cad`, `user_alt`, `dt_cad`, `dt_alt`) VALUES
(18, 13, 0, 'Danielle catarine machado oliveira santos', '2016-02-12 12:22:00', '105.120.014-80', '(81) 8812-2208', '(81) 98812-2208', '', 'Obs: USG venoso com doppler do membro inferior esquerdo', 'E', '12', '11', '2016-02-12', '2016-02-16'),
(19, 13, 0, 'Danielle Catarine Machado Oliveira Santos', '2016-02-15 14:00:00', '105.120.014-80', '(81) 8414-2716', '(81) 98414-2716', '', 'Obs: USG venoso com doppler do membro inferior esquerdo', 'A', '12', '10', '2016-02-12', '2016-02-12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `cd_user` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `login` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `home_page` varchar(200) DEFAULT NULL,
  `telefone` varchar(40) DEFAULT NULL,
  `celular` varchar(40) DEFAULT NULL,
  `endereco` varchar(150) DEFAULT NULL,
  `bairro` varchar(20) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  `observacao` varchar(550) DEFAULT NULL,
  `tp_solicitacao` varchar(100) DEFAULT NULL,
  `perfil` varchar(100) DEFAULT NULL,
  `cd_medico` varchar(100) DEFAULT NULL,
  `usr_cad` varchar(20) NOT NULL,
  `usr_alt` varchar(20) NOT NULL,
  `dt_cad` date NOT NULL,
  `dt_alt` date NOT NULL,
  `status` varchar(5) NOT NULL,
  `img_perfil` varchar(255) DEFAULT NULL,
  `cep` varchar(200) DEFAULT NULL,
  `vinculo_medico` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`cd_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`cd_user`, `nome`, `login`, `senha`, `email`, `home_page`, `telefone`, `celular`, `endereco`, `bairro`, `cidade`, `estado`, `observacao`, `tp_solicitacao`, `perfil`, `cd_medico`, `usr_cad`, `usr_alt`, `dt_cad`, `dt_alt`, `status`, `img_perfil`, `cep`, `vinculo_medico`) VALUES
(8, 'adm@com', 'adm@com', '123456', 'theleoneoliveira@gma', '', '(00) 0000-0000', '(00) 00000-0000', 'rua campos sales 196', 'madalen', 'recife', 'PE', '', 'S', '1', '', '3', '8', '2016-01-06', '2016-01-20', 'E', '24e06f9b140c5ed6a0962366d86c8d84.jpg', '50610-400', '2'),
(9, 'Jose Emilio', 'emilio', 'bip5557', 'emilio@ig.com.br', '', '(81) 9717-0102', '(81) 99717-0102', 'Av Presdente Dutra 298', 'Imbiribeira', 'Recife', 'PE', 'Administrador', 'S', '1', '', '8', '10', '2016-01-20', '2016-02-12', 'A', 'e290b3fb956ddbb8048de22122921bbd.jpg', '51190-900', '2'),
(10, 'Micheline Ebrahim', 'micheline', 'moroemaldeia', '', '', '', '', '', '', '', '', 'Medico executante de ultrassonografia.', 'S', '1', '12', '9', '', '2016-01-27', '0000-00-00', 'A', '', '', '1'),
(11, 'Wiviane Ebrahim', 'Wiviane', 'adm1234', 'wiviane@me.med.br', '', '', '(81) 99720-578', 'Av Presidente Dutra.298', 'Imbiribeira', 'Recife', 'PE', 'Funcionaria ', 'N', '2', '', '9', '10', '2016-01-27', '2016-02-10', 'A', '', '51190-900', '2'),
(12, 'ClinicaJordao', 'clinicajordao', 'clinica12', 'lourdesgurgel@hotmai', '', '(81) ', '(81) 99717-0102', 'Jordao', 'Jordao', 'Recife', 'PE', '', 'S', '3', '', '10', '', '2016-02-09', '0000-00-00', 'A', '', '51190-900', '2'),
(13, 'Clinica Bem-estar', 'clinicabemestar', 'clinica15', 'micheline@ebrahim.co', 'micheline@ebrahim.com.br', '(81) 3428-6965', '(81) 98788-5479', 'Avenida São Miguel, 630', 'Afogados', 'Recife', 'PE', '', 'S', '3', '', '10', '10', '2016-02-13', '2016-02-18', 'A', '', '50770-720', '2');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
